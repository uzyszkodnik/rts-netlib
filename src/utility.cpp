#include "utility.h"
#include "hiddenconf.h"
using namespace std;

uint64_t ms_since_epoch(){
    auto current_time = std::chrono::system_clock::now().time_since_epoch();
    uint64_t milliseconds_since_epoch = std::chrono::duration_cast<std::chrono::milliseconds>(current_time).count();
    return milliseconds_since_epoch;
}


void PeriodicCaller::callback_runner(const boost::system::error_code& ec){


    if(!ec){
        period_timer.expires_from_now(boost::posix_time::milliseconds(period));

        for(auto &callback: callbacks){
            callback(); 
        } 

        period_timer.async_wait(std::bind(&PeriodicCaller::callback_runner,this,std::placeholders::_1));

    }else if(ec != boost::asio::error::operation_aborted){
        puts("error @ callback runner"); 
    } 

}
