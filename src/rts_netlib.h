#pragma once

#include "config.h"
#include <vector>
#include <stdint.h>
#include <utility>
#include <functional>
#include <vector>
#include <memory>
#include "comm_protocol.h"
#include <tuple>
#include <list>
#include <condition_variable>
#include<deque>

#ifdef RTS_NETLIB_DEBUG
    #include <boost/crc.hpp>
#endif

const uint32_t mtu = mtu_size - sizeof(data_packet);
enum NETLIB_CMD{
    PLAYER_DISCONNECTED=0
    ,PLAYER_RECONNECTED
    ,SIMULATION_RESUMED 
    ,SIMULATION_DISCONNECTED
    ,SIMULATION_PAUSED
    ,SIMULATION_UNPAUSED
    ,PLAYER_DROPPED
    ,SIMULATION_STARTED
    ,PACE_REQUEST
    ,STOP_REQUEST
    ,START_REQUEST
    ,SERVER_TURN
    ,SERVER_TURN_LEN
    ,TURN_HEADER
    ,TURN_FRAGMENT
    ,AUTH_FAILED
    ,AUTH_SUCCESS
    ,NONE
};

struct netlib_msg{
    netlib_msg(){type=NONE;}
    netlib_msg(NETLIB_CMD cmd_type) {type = cmd_type;};
    uint32_t type;
};

struct disconnection_msg: netlib_msg{
    disconnection_msg(uint32_t id) : netlib_msg(PLAYER_DISCONNECTED), player_id(id){};
    uint32_t player_id;
};

struct player_drop_msg: netlib_msg{
    player_drop_msg(uint32_t id) : netlib_msg(PLAYER_DROPPED), player_id(id){};
    uint32_t player_id;
};

struct player_reconnection_msg: netlib_msg{
    player_reconnection_msg(uint32_t id) : netlib_msg(PLAYER_RECONNECTED), player_id(id){};
    uint32_t player_id;

};

struct server_turn_info : netlib_msg{
    server_turn_info(uint32_t t): netlib_msg(SERVER_TURN), turn(t){};
    uint32_t turn;
};

struct server_turn_len: netlib_msg{
    server_turn_len(uint32_t l) : netlib_msg(SERVER_TURN_LEN), turn_len(l){};
    uint32_t turn_len;
};

struct pause_msg: netlib_msg{
    pause_msg(uint32_t id) : netlib_msg(SIMULATION_PAUSED), player_id(id){}; 
    uint32_t player_id;
};

struct sim_pace_request: netlib_msg{
    sim_pace_request(uint32_t _turn_time): netlib_msg(PACE_REQUEST), turn_time(_turn_time){}
    uint32_t turn_time;
};

enum CLIENT_TYPE{
    UI_CLIENT=0,
    SIM_CLIENT, 

};

struct command{
    uint32_t size;
    uint32_t player_id;
    char data[1];
};

struct turn_fragment: netlib_msg{
    uint32_t command_count;
    command commands[];
};

//this is important this is the size of cmd which is guaranteed to be sent,
//send a byte more and some malicious router will drop it forever
const uint32_t max_cmd_size = mtu - sizeof(turn_fragment) - sizeof(command);

struct turn_header_pkg: netlib_msg{


#ifdef RTS_NETLIB_DEBUG
    uint32_t crc;
#endif
    //relative time when the server wants the pkg to get scheduled on servers part
    //so to get when we want to sched we need to substract ping from it
    uint32_t schedule_time;
    uint32_t followup_count;
    uint32_t turn_id;
    uint32_t cmd_count;

    turn_fragment data_fragment;
};

struct connection_info{
    uint32_t client_type;
    uint32_t id;
    uint32_t turn;
    uint64_t password;
};


class Turn{
    private:
        std::list<std::shared_ptr<std::vector<char>>> pkg_chain;
        turn_header_pkg* header=nullptr;
        char* write_pos=nullptr;
        char* write_end=nullptr;
        BufferCache<char>& cache;
        turn_fragment* current_fragment=nullptr;
        void write_command(uint32_t player_id, const char* data, uint32_t len);

    public:
#ifdef RTS_NETLIB_DEBUG
        void add_crc(uint32_t crc){header->crc=crc;};
#endif
        Turn(BufferCache<char>& c,uint32_t turn_id);
        void add_command(uint32_t player_id, const char* data,uint32_t len);
        void set_expected_time(uint32_t time);
        const std::list<std::shared_ptr<std::vector<char>>>& get_turn_pkgs(){return pkg_chain;};

};

struct turn_command{
    turn_command(): player_id(0),size(0),command(nullptr){};
    turn_command(uint32_t id, uint32_t _size, std::shared_ptr<std::vector<char>>& buf) : 
        player_id(id), size(_size), command(buf){};
    uint32_t player_id;
    uint32_t size;
    std::shared_ptr<std::vector<char>> command;
};

class TurnUnpacker{
    private:
        std::shared_ptr<std::vector<turn_command>> packets;
        uint32_t time=0; 
        uint32_t followup_count = 0; ;
        void process_header(const char* data,uint32_t size);
        void process_fragment(const char* data,uint32_t size);
        uint32_t turn_id = 0; 
        bool waiting_for_header=true;
        BufferCache<char>& cache;
        BufferCache<turn_command>& cmd_cache;
#ifdef RTS_NETLIB_DEBUG
        uint32_t crc=0;
#endif
    public:
        //returns whether the packet was the last one for this turn
        TurnUnpacker(BufferCache<char>& _cache,BufferCache<turn_command>& _cmd_cache): cache(_cache),cmd_cache(_cmd_cache){};
        bool process_packet(const char* data, uint32_t len);
        //tuple(player_id,command_size,command)
        std::shared_ptr<std::vector<turn_command>> get_cmds(){auto tmp = packets; packets.reset();return tmp;};
        void clear();
        //returns delta time for scheduling the msg @ server
        uint32_t get_schedule_time(){return time;};
        uint32_t get_turn(){return turn_id;};
#ifdef RTS_NETLIB_DEBUG
        uint32_t checksum(){return crc;};
#endif
};


class RtsClient{
    private:
        uint64_t password;
        std::shared_ptr<boost::asio::io_service> io_service;

        std::string ip;
        std::string port;

        std::mutex connection_lock;
        std::condition_variable awaiting_connection;

        void await_connection(const char* data , uint32_t len);
        bool auth_performed=false;
    protected:
        uint32_t id;
        CLIENT_TYPE type;
        std::unique_ptr<CommLinkClient> client;
        bool connected = false;
        uint32_t current_turn=0;
        safe_function<void(const char*,uint32_t)> recv_callback ;
        safe_function<void(const netlib_event&)> event_callback;
    public:
        RtsClient(std::shared_ptr<boost::asio::io_service>& _io_service, uint32_t _id,uint64_t _password);
        //thread unsafe
        void set_event_callback(std::function<void(const netlib_event&)> f){event_callback = f ; };
        bool connect(const std::string& addr, const std::string& port);
        bool reconnect(){return connect(ip,port);};
        void disconnect(){client->disconnect(); disconnect_quietly();};
        uint64_t get_ping(){return client->get_ping();};

        //doesnt inform server about the dc mainly for testing purposes
        void disconnect_quietly();
        ~RtsClient(){};


};

class RtsUIClient: public RtsClient{
    public:
        RtsUIClient(std::shared_ptr<boost::asio::io_service>& _io_service, uint32_t _id,uint64_t _password):
        RtsClient(_io_service,_id,_password){type=UI_CLIENT;};
        //size shouldnt be higher than max_cmd_size
        inline void send_command(const char* cmd, uint32_t len){client->send_data(cmd,len);};

        void set_network_event_callback(std::function<void(const char* data, uint32_t len)> f){recv_callback=f; if(connected) client->set_recv_callback(f);};
        void set_event_callback(std::function<void(const netlib_event&)> f){event_callback = f; if(connected) client->set_event_callback(f);};

};

//tracks time since some event;
class SimTimeTracker{
    private:
        uint64_t start_time;
    public:
        SimTimeTracker() : start_time(ms_since_epoch()){};
        uint64_t get_tss(){
            return uint32_t(ms_since_epoch() -start_time); 
        };
        void start(){
            start_time = ms_since_epoch(); 
        };

};



class RtsSimClient: public RtsClient{
    private:
        bool event_occured = false;

        std::function<void(netlib_msg)> callback;
        uint32_t valid_step=0;
        void recv_handler(const char* data,uint32_t len);
        SimTimeTracker sim_time;

        std::mutex sim_lock;
        std::condition_variable turn_ready;

        enum SIM_STATE{
           NORMAL,
           CATCHING_UP,
           SLOWED, 
           REPLAYING,
        };
        SIM_STATE client_state;
        uint32_t original_turn_len=100000;

        uint32_t current_slowdown=0;
        uint32_t server_turn = 0; 
        BufferCache<turn_command> cmd_cache; 
        BufferCache<char> buffer_cache;

        //list of commands
        //optimally size of 1
        //it grows in size when some kind of lag occures or we are replaying
#ifdef RTS_NETLIB_DEBUG
        boost::crc_32_type crc;
        std::deque<
            //for current turn
            //sched_time, cmds for turn,checksum
            std::tuple<uint64_t,std::shared_ptr<std::vector<turn_command>>,uint32_t>> turn_commands;
#else 
        std::deque<
            //for current turn
            //sched_time, cmds for turn
            std::tuple<uint64_t,std::shared_ptr<std::vector<turn_command>>>> turn_commands;
#endif


                TurnUnpacker unpacker;

        netlib_event last_msg;

        void netevent_callback(const netlib_event& ev);
    public: 
        RtsSimClient(std::shared_ptr<boost::asio::io_service>& _io_service, uint32_t _id,uint64_t _password):
        RtsClient(_io_service,_id,_password), client_state(NORMAL),cmd_cache(10,10),buffer_cache(30,mtu),unpacker(buffer_cache,cmd_cache)
        {
            type=SIM_CLIENT;
            recv_callback=std::bind(&RtsSimClient::recv_handler,this,std::placeholders::_1, std::placeholders::_2);
            event_callback=std::bind(&RtsSimClient::netevent_callback,this,std::placeholders::_1);
        }

        //calls function f upon events such as disconnection, reconnection, 
        //desync etc - check EVENT enum for details
        //unworking atm
        void set_network_event_callback(std::function<void(netlib_msg)> _callback){callback = _callback;};

        //blocks until next step of simulation
        //returns:
        //and then  returns set of commands for the step
        //or a nullptr when received a signal or an error has occured
        std::shared_ptr<std::vector<turn_command>> receive_commands(); 
        //call it after processing the commands , it will return them to cache for reuse
        void free_cmds(std::shared_ptr<std::vector<turn_command>> cmd );

        uint32_t get_current_step();
        bool connect(const std::string& ip,const std::string& port);
        bool reconnect();
        //signal simulation to unlock;
        void signal(){event_occured = true; turn_ready.notify_one();};


        //testing purposes
        uint32_t get_server_turn_len() const {return original_turn_len;};
        //call me when receive_commands returns nullptr
        const netlib_event& get_last_msg() const {return last_msg;};
};


class TurnPaceTracker{
    private:
        uint32_t original_turn_len;
        uint32_t current_turn_len;
        uint32_t maximal_slowdown;
        std::vector<uint32_t> pace_requests;
        void update_turn_len();
    public:

        TurnPaceTracker(uint32_t turn_len, uint32_t _maximal_slowdown,uint32_t player_count);
        bool add_slowdown(uint32_t player_id, uint32_t value);
        void remove_client(uint32_t player_id);
        inline uint32_t get_turn_len(){return current_turn_len;};


};


class PauseActivitiesTracker{
    private:
        uint32_t max_time;
        std::vector<uint32_t> player_times;
        std::vector<bool> pausing;
        uint32_t update_time;


        boost::asio::deadline_timer tracker_timer;
        //time_to removal, removal_callback - called when the activity is replaced or it timed out
        std::map<uint32_t, std::tuple<uint32_t, std::function<void()>>> activities;

        void update_pause();
        void update();
    public:
        PauseActivitiesTracker(boost::asio::io_service& io, uint32_t _max_time,uint32_t player_count, uint32_t _update_time=1000) : max_time(_max_time), player_times(player_count,_max_time), pausing(player_count,false),update_time(_update_time), tracker_timer(io){
            tracker_timer.expires_from_now(boost::posix_time::milliseconds(0));
            tracker_timer.async_wait(std::bind(&PauseActivitiesTracker::update,this)); 
        };
        void add_activity(uint32_t player_id, std::function<void()> removal_callback);
        void remove_activity(uint32_t player_id){activities.erase(player_id);pausing[player_id]=false;};
        size_t pausing_clients(){return activities.size();};

};
class RtsServer{
    private:
#ifdef RTS_NETLIB_DEBUG
        boost::crc_32_type turn_crc;
#endif
        const uint32_t periodic_update_interval = 100;

        const uint32_t max_pause_period = 1000*60;


        uint32_t connected_clients = 0 ;
        uint32_t pausing_clients = 0 ;

        BufferCache<char> cache;
        uint64_t pending_id=0;
        const uint32_t cleanup_interval=100;
        const uint32_t time_for_auth = 2000;
        std::shared_ptr<boost::asio::io_service> io_service;

        //[client_id][client_type]
        std::vector<std::vector<std::shared_ptr<CommLinkServer>>> clients;

        std::vector<bool> banned;

        std::vector<std::shared_ptr<CommLinkServer>> dead_sockets;

        //tuple time_to_removal, pointer
        //holds udp connections which have not been authenticated
        std::map<uint64_t,std::tuple<uint32_t, std::shared_ptr<CommLinkServer>> > pending_connections; 

        boost::asio::ip::tcp::socket s;
        boost::asio::ip::tcp::acceptor a;
        std::vector<uint64_t> passwords;

        void accept_handler(const boost::system::error_code& ec);
        void sim_recv_handler(uint32_t id,const char* data,uint32_t len);
        void dc_handler(uint32_t id);
        void ui_recv_handler(uint32_t id,const char* data,uint32_t len);

        void dead_sockets_cleanup();
        boost::asio::deadline_timer turn_timer;

        void connection_phase_dc(const netlib_event&ev,uint32_t id, uint32_t client_type);
        void start_simulation();
        void turn_callback(const boost::system::error_code& ec);
        uint32_t session_len ;
        uint32_t pkg_size_bound;
        std::shared_ptr<Turn> current_turn;
        std::deque<std::shared_ptr<Turn>> past_turns;
        uint32_t turn_data_size=0;
        uint64_t get_max_ping(CLIENT_TYPE type);
        void client_dc_callback(const netlib_event& ev,uint32_t client_id, uint32_t type);
        void send_to_connected_clients(const char* data, uint32_t len,CLIENT_TYPE type);
        void remove_client(uint32_t client_id);
        void continue_simulation();
        uint32_t turn_id = 0 ;
        TurnPaceTracker game_pace;
        bool auth_client(connection_info* auth);
        void handle_replay_request(uint32_t player_id, uint32_t replay_point);

        enum SERVER_STATE{
            AWAITING_START, 
            SIMULATION, 
            PAUSED 
            } ;

        SERVER_STATE server_state = AWAITING_START;

        std::function<void(uint32_t,const char*, uint32_t )> ui_callback= [](uint32_t,const char*, uint32_t){};
        std::function<void(uint32_t,const char*,uint32_t)> sim_callback= [](uint32_t,const char*, uint32_t){};
        std::function<void(const netlib_event&, uint32_t,uint32_t)> dc_callback;

        void set_sim_data_callbacks();
        void run_simulation();
        bool client_is_connected(uint32_t client_id);
        void pause_handler();
        void auth_udp(uint64_t cache_id, const char* data, uint32_t len);
        PeriodicCaller periodic_tasks;
        PauseActivitiesTracker pause_activities;
        uint32_t turn_len;
        void pause(uint32_t client_id);

        //TODO:needed @auth, it will be removed after merge the Rts*Client classes since they both
        //provide now thread safety eh
        std::vector<uint32_t> current_player_turns;
        safe_function<void(const netlib_event&)> event_callback;
    public:

        //passwords[i] = authentication password for player with id = i 
        //avg_session_len - upper bound on length of game session in minutes - used for cache allocation
        //packets_per_second = assumed number of packets per second per player
        RtsServer( std::shared_ptr<boost::asio::io_service>& _io_service, const std::string& port,const std::vector<uint64_t>& _passwords, uint32_t _turn_ms_len=100,uint32_t max_slowdown=300 ,uint32_t _session_len=30,uint32_t _pkg_size_bound=50); 

        //starts the server and immediately returns
        void run();


        //called when all players disconnect
        void set_event_callback(std::function<void(const netlib_event&)>f){event_callback=f;};
};
