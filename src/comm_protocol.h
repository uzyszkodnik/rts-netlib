#pragma once
#include "config.h"
#include <functional>
#include <stdint.h>
#include <boost/asio.hpp>
#include <memory>
#include <set>
#include <vector>
#include <deque>
#include <tuple>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include "utility.h"


const uint32_t mtu_size = 576;

enum PACKET_TYPE{
    DATA = 0
        ,ACK
        ,PING
        ,PING_RESPONSE
        ,DISCONNECT
};

//TODO: alignment crap
struct  packet{
    uint32_t type;
};

struct ping_packet : packet{
    uint64_t sending_time;  
};

struct data_packet : packet{
    inline static size_t header_size();
    uint32_t seq_num; 
    char data[];
};

struct ack_packet : packet{
    uint32_t ack_num;
};


enum NETLIB_EVENT_CODE{
    CONNECTED,
    CONNECTING_TIMED_OUT,
    DISCONNECTED,
    FAILED_AUTH,
    BOOST_ERROR,
    SIMULATION_FINISHED,
    NOTHING,
};

struct netlib_event{
    netlib_event(){event=NOTHING;};
    netlib_event(NETLIB_EVENT_CODE ev) : event(ev), ec(boost::system::posix_error::make_error_code(boost::system::errc::success)){};
    netlib_event(NETLIB_EVENT_CODE ev, const boost::system::error_code& err): event(ev), ec(err){};
    NETLIB_EVENT_CODE event;
    boost::system::error_code ec;
};

inline bool operator==(const netlib_event& a, NETLIB_EVENT_CODE code){return a.event == code;};
inline bool operator==(NETLIB_EVENT_CODE code,const netlib_event& a){return a.event == code;};
inline bool operator==(const netlib_event& a, const netlib_event& b){return a.event!=BOOST_ERROR?a.event==b.event:a.ec==b.ec;};
inline bool operator!=(const netlib_event& a, NETLIB_EVENT_CODE code){return !(a==code);};
inline bool operator!=(NETLIB_EVENT_CODE code,const netlib_event& a){return !(a== code);};
inline bool operator!=(const netlib_event& a, const netlib_event& b){return !(a==b);};

class CommLink{
    protected:
        std::shared_ptr<boost::asio::io_service> io; 
        bool connected;
        boost::asio::ip::udp::socket socket_;
        boost::asio::ip::udp::endpoint endpoint;
        void send_ping_packet();
        std::vector<char> input_buffer;
        boost::asio::ip::udp::endpoint sender;
        void process_ping(std::vector<char>& buf);

        //hang on recv as long as we dont get ping packet
        void await_ping(std::function<bool()> action,const boost::system::error_code& ec, size_t size);
        //hangs on recv as long as we dont get response packet
        void await_ping_response(std::function<bool()> action,const boost::system::error_code& ec, size_t size);

        //starts the normal communication
        //ping responses, data recv etc
        void start_comm_protocol();

        safe_function<void(const netlib_event&)> event_callback;
    private:
        std::mutex dc_lock;
        std::condition_variable awaiting_dc;
        uint64_t update_interval;
        std::atomic<uint64_t> current_ping;
        // uint64_t current_ping;
        uint64_t delay_for_dc = 2000;
        uint64_t heartbeat_interval=100;
        uint32_t sent_counter = 0; 
        uint32_t conseq_recv=0;
        uint64_t ping_interval=100;
        uint64_t ts_last_packet;
        safe_function<void(const char* ,uint32_t)> recv_callback;
        safe_function<void(char*,uint32_t)> delay_callback;

        BufferCache_safe<char> buffer_cache;
        std::map<uint32_t, 
            std::tuple< std::shared_ptr<std::vector<char>> , size_t>
                > incoming_package_storage;

        std::map<uint32_t, 
            std::tuple<std::shared_ptr<std::vector<char>>,uint32_t,uint64_t> 
                > unconfirmed_packets;

        boost::asio::deadline_timer packet_resend_timer;
        boost::asio::deadline_timer ping_timer;
        boost::asio::deadline_timer heartbeat_timer;
        char watch;
        void send_handler(std::shared_ptr<std::vector<char>> buf,bool,const boost::system::error_code& err, size_t bytes_transferred);
        void read_handler(const boost::system::error_code& err, std::size_t transferred);
        void do_recv();
        void send_packet(std::shared_ptr<std::vector<char>> buf,size_t size, bool return_buffer=true);

        void acknowledge_packet(uint32_t seq_num);
        void send_ping(const boost::system::error_code& ec);
        void respond_ping(std::vector<char>& buf,size_t len);
        void handle_data(const std::vector<char>& data, size_t size);
        void monitor_unconfirmed(const boost::system::error_code& ec);
        void heartbeat_callback(const boost::system::error_code& ec);
        void handle_ack(std::vector<char>&data);

        void handle_dc();
        //TODO: add mtu discovery
    
    public: 
        uint64_t get_ping(){ return current_ping.load();};
        CommLink(std::shared_ptr<boost::asio::io_service>& io_service ,boost::asio::ip::udp::endpoint endp, uint64_t _update_interval); 
        //dont send more than CommLink.mtu_size bytes
        //thread safe
        void send_data(const char* data, uint32_t len);
        //thread unsafe call only from the io_service thread
        void send_data_unsafe(const char *data, uint32_t len);
        //callback may change contents but cant change the size
        //called when a delay in sending occured
        //set callbacks only while the io_service is not running
        //thread unsafe
        //dont use them inside io_service context
        void set_delay_callback(std::function<void(char* data,uint32_t len )> callback){io->post([=]{delay_callback=callback;});};
        void set_recv_callback(std::function<void(const char*, uint32_t)> callback){io->post([=]{recv_callback=callback;});};
        void set_event_callback(std::function<void(const netlib_event&)> f){io->post([=]{event_callback=f;});};

        //thread unsafe versions
        //use in io_service context only
        void set_delay_callback_unsafe(std::function<void(char* data,uint32_t len )> callback){delay_callback = callback;};
        void set_recv_callback_unsafe(std::function<void(const char*, uint32_t)> callback){recv_callback = callback;};
        void set_event_callback_unsafe(std::function<void(const netlib_event&)> f){event_callback=f;};
        //you cant reconnect after dc
        void disconnect_unsafe();
        void disconnect();

        //dont delete outside of io_service context
        

};


class CommLinkClient: public CommLink{
    private:
        std::mutex connection_lock;
        std::condition_variable awaiting_connection;
        void await_connection(const boost::system::error_code& ec, size_t size);
    public: 
        CommLinkClient(std::shared_ptr<boost::asio::io_service>& io_service,uint64_t _update_interval=100);
        bool connect(const std::string& ip , const std::string port);

};

class CommLinkServer: public CommLink{
    private:
        void await_connection(const boost::system::error_code& ec, size_t size);
    public:
        CommLinkServer(std::shared_ptr<boost::asio::io_service>& io_service, short port=0,uint64_t _update_interval=100);
         uint16_t get_port(){return socket_.local_endpoint().port();};

};
