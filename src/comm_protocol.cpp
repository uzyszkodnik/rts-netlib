
#include "hiddenconf.h"
#include "comm_protocol.h"
#include <algorithm>
#include <stdio.h>
#include <functional>
#include <chrono>
#include <tuple>
#include <boost/date_time/posix_time/posix_time.hpp> 


#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

using namespace boost::asio::ip;
using namespace std;
using namespace std::placeholders;

CommLink::CommLink(std::shared_ptr<boost::asio::io_service>& io_service_ ,boost::asio::ip::udp::endpoint endp,uint64_t _update_interval) : 
    io(io_service_)
    ,socket_(*io,endp)
    ,input_buffer(mtu_size)
    ,update_interval(_update_interval)
    ,buffer_cache(50, mtu_size)
    ,packet_resend_timer(*io)
    ,ping_timer(*io)
    ,heartbeat_timer(*io)
{
    connected=false;
    current_ping=100;
    recv_callback = [](const char *, uint32_t){};
    delay_callback = [](char *, uint32_t){};
    event_callback = [](const netlib_event&){};
}

void CommLink::start_comm_protocol()
{
    connected = true;

    do_recv();
    packet_resend_timer.expires_from_now(boost::posix_time::milliseconds(update_interval));

    packet_resend_timer.async_wait(bind(&CommLink::monitor_unconfirmed,this,_1)); 

    ts_last_packet = ms_since_epoch();

    ping_timer.expires_from_now(boost::posix_time::seconds(0));
    ping_timer.async_wait(bind(&CommLink::send_ping,this,_1));

    heartbeat_timer.expires_from_now(boost::posix_time::milliseconds(heartbeat_interval));
    heartbeat_timer.async_wait(bind(&CommLink::heartbeat_callback,this,_1));


}


void CommLink::handle_dc()
{
    event_callback(netlib_event(DISCONNECTED));
    disconnect_unsafe();
}

void CommLink::disconnect_unsafe()
{
    if(connected){
#ifdef RTS_NETLIB_DEBUG
	BOOST_LOG_TRIVIAL(debug)<<"disconnect_unsafe called";
#endif

        packet dc_msg;
        dc_msg.type = DISCONNECT; 
        //sync since we're shutting down the socket
        socket_.send_to(boost::asio::buffer((char *)&dc_msg,sizeof(dc_msg)),endpoint);
        connected = false;
        //shutdown socket and communication facilities
        socket_.cancel();
        packet_resend_timer.cancel();
        heartbeat_timer.cancel();
        ping_timer.cancel();

#ifdef RTS_NETLIB_DEBUG
	BOOST_LOG_TRIVIAL(debug)<<"disconnect_unsafe ended";
#endif


    }
}

void CommLink::disconnect()
{
    unique_lock<mutex> l(dc_lock);
    io->post([=]{
             unique_lock<mutex> m_l(dc_lock);
             disconnect_unsafe();
             m_l.unlock();
             awaiting_dc.notify_one(); 
#ifdef RTS_NETLIB_DEBUG
	BOOST_LOG_TRIVIAL(debug)<<"unlocked";
#endif

   
            });
#ifdef RTS_NETLIB_DEBUG
	BOOST_LOG_TRIVIAL(debug)<<"disconnect safe waiting";
#endif

    awaiting_dc.wait_for(l,chrono::seconds(10), [=]{return !connected;});
#ifdef RTS_NETLIB_DEBUG
	BOOST_LOG_TRIVIAL(debug)<<"disconnect safe stopped waiting";
#endif

}

void CommLink::heartbeat_callback(const boost::system::error_code& ec)
{
    if(!ec){
        if(ms_since_epoch()-ts_last_packet >= delay_for_dc && connected){

            handle_dc();

        } else{
            heartbeat_timer.expires_from_now(boost::posix_time::milliseconds(heartbeat_interval));
            heartbeat_timer.async_wait(bind(&CommLink::heartbeat_callback,this,_1));

        }

    }else if(ec != boost::asio::error::operation_aborted){
        BOOST_LOG_TRIVIAL(error)<<"error @ heartbeat callback"<<ec.message();
        event_callback(netlib_event(BOOST_ERROR,ec));
    }


}



CommLinkClient::CommLinkClient(std::shared_ptr<boost::asio::io_service>& io_service,uint64_t _update_interval): CommLink(io_service, boost::asio::ip::udp::endpoint(
            boost::asio::ip::udp::v4(), 0),_update_interval) {
    
};

void CommLink::await_ping(function<bool()> on_recv, const boost::system::error_code& ec, size_t size)
{

    if(!ec){

        bool received = false;

        if(size>=sizeof(ping_packet)){
            ping_packet *p = (ping_packet *) input_buffer.data();
            if(p->type == PING){
                //meh
                //respond_ping is using internal var endpoint
                //so in order to respond we need to set that var 
                //in other words whoever sends us a ping gets a response
                //but is not guaranteed to get the connection
                //otherwise we could implement some simple form of auth for the pings
                //or change the respond_ping but this is quicker
                
                auto tmp = endpoint;
                endpoint = sender;
                respond_ping(input_buffer,size); 
                endpoint =tmp;

                received = on_recv();
            }
        }

        if(!received){
            socket_.async_receive_from( boost::asio::buffer(input_buffer),
            sender,
            bind(&CommLink::await_ping,this,on_recv,placeholders::_1,placeholders::_2)
            );
        } 
    
    }
   
}

void CommLink::await_ping_response(function<bool()> on_recv,const boost::system::error_code& ec, size_t size)
{

    if(!ec){
        bool received = false;

        if(size>=sizeof(ping_packet)){
            ping_packet *p = (ping_packet *) input_buffer.data();
            if(p->type == PING_RESPONSE){
                process_ping(input_buffer); 
                received = on_recv();
            }
        }

        if(!received){
            socket_.async_receive_from( boost::asio::buffer(input_buffer),
            sender,
            bind(&CommLink::await_ping_response,this,on_recv,placeholders::_1,placeholders::_2)
            );
        } 
    
    }else{
        event_callback(netlib_event(BOOST_ERROR,ec)); 
    }

}

//we're sending ping packet to the server, and when it responds with a ping packet of its own we
//assume the connection has been estabilished
bool CommLinkClient::connect(const std::string& ip , const std::string port)
{

    //doesnt support reconnects
    std::unique_lock<mutex> l(connection_lock);
    //just in case eh
    if(!connected){

        io->post([=]()
        {
            udp::resolver resolver(*io);
            endpoint = *resolver.resolve({udp::v4(), ip ,port});

            socket_.async_receive_from( boost::asio::buffer(input_buffer),
            sender,
            bind(&CommLinkClient::await_connection,this,placeholders::_1,placeholders::_2)
            );

            send_ping_packet();

        });

        awaiting_connection.wait_for(l,chrono::seconds(2), [this](){return connected;}); 
    }

    return connected;
}

void CommLink :: send_packet(shared_ptr<vector<char>> buf,size_t size, bool return_buffer)
{

    socket_.async_send_to(boost::asio::buffer(*buf,size),endpoint, bind(&CommLink::send_handler,this,buf,return_buffer,_1,_2)); 
}

void CommLink :: monitor_unconfirmed(const boost::system::error_code& ec)
{

    if(!ec){

        for(auto& i: unconfirmed_packets){
            shared_ptr<vector<char>> b;
            uint32_t time_left;
            uint32_t data_len;
            tie(b,data_len,time_left) = i.second; 
            if(time_left <= update_interval){
                data_packet *p =(data_packet *) b->data();
                delay_callback(p->data,data_len);
                get<2>(i.second) = current_ping; 

                send_packet(b,data_packet::header_size()+data_len, false); 
            }else{
                get<2>(i.second) -= update_interval; 
            }

        }

        packet_resend_timer.expires_from_now(boost::posix_time::milliseconds(update_interval));
        packet_resend_timer.async_wait(bind(&CommLink::monitor_unconfirmed,this,_1)); 


    }else if(ec != boost::asio::error::operation_aborted ){
        BOOST_LOG_TRIVIAL(error)<<"error @monitor unconfirmed: "<<ec.message();
        event_callback(netlib_event(BOOST_ERROR,ec));

    }


}
void CommLink :: send_data(const char* data, uint32_t len )
{
    data_packet *p;
    size_t packet_size = data_packet::header_size()+len;
    auto buffer = buffer_cache.get_buffer(packet_size);
    p = (data_packet *) buffer->data();
    p->type=DATA;
    p->seq_num = sent_counter++;
    copy(data,data+len, p->data);
    //run the volatile stuff in io_thread context
    //cant be more fucking thread safe
    io->post(
           [=](){ 
                unconfirmed_packets[p->seq_num]=make_tuple(buffer,len,current_ping.load());
                send_packet(buffer,packet_size,false); 
            } 
    );

}

void CommLink :: send_data_unsafe(const char* data, uint32_t len )
{
    data_packet *p;
    size_t packet_size = data_packet::header_size()+len;
    auto buffer = buffer_cache.get_buffer(packet_size);
    p = (data_packet *) buffer->data();
    p->type=DATA;
    p->seq_num = sent_counter++;
    copy(data,data+len, p->data);

    unconfirmed_packets[p->seq_num]=make_tuple(buffer,len,current_ping.load());
    send_packet(buffer,packet_size,false); 

}

void CommLink::acknowledge_packet(uint32_t seq_num)
{
    ack_packet *p;
    size_t packet_size = sizeof(ack_packet);
    auto buffer = buffer_cache.get_buffer(packet_size);
    p = (ack_packet *) buffer->data();
    p->type = ACK;
    p->ack_num = seq_num;
    send_packet(buffer,sizeof(ack_packet));
}

void CommLink::respond_ping(vector<char>& buf,size_t len)
{
    size_t packet_size = sizeof(ping_packet);
    if(len>=packet_size){
        ping_packet *p;
        ping_packet *response;

        p = (ping_packet *)buf.data();
        auto b = buffer_cache.get_buffer(packet_size);
        response = (ping_packet*) b->data();

        response->type=PING_RESPONSE;
        response->sending_time = p->sending_time;

        send_packet(b, sizeof(ping_packet));
    }
}

void CommLink::send_ping_packet()
{
    ping_packet *p; 
    size_t packet_size = sizeof(ping_packet);

    auto buffer = buffer_cache.get_buffer(packet_size);

    p = (ping_packet *) buffer->data();

    p->type=PING;
    p->sending_time =  ms_since_epoch();

    send_packet(buffer,packet_size);
}

void CommLink::send_ping(const boost::system::error_code& ec)
{
    if(!ec ){
        if(connected){
            send_ping_packet();
        }

        ping_timer.expires_from_now(boost::posix_time::milliseconds(ping_interval));
        ping_timer.async_wait(bind(&CommLink::send_ping,this,_1));

    }else if(ec != boost::asio::error::operation_aborted ){
        BOOST_LOG_TRIVIAL(error)<<"error @ send_ping: "<<ec.message()<<" "<<strerror(errno);
        event_callback(netlib_event(BOOST_ERROR,ec));
    }

}

void CommLink::process_ping(vector<char>& buf)
{
    ping_packet *p = (ping_packet *)buf.data();
    current_ping = (ms_since_epoch()-p->sending_time)/2;
}

void CommLink :: send_handler(shared_ptr<vector<char>> buf, bool perform_free,const boost::system::error_code& err, size_t bytes_transferred)
{

    if(!err){

    }else if(err != boost::asio::error::operation_aborted ){
        BOOST_LOG_TRIVIAL(error)<<"error @ send_handler: "<<err.message()<<" "<<strerror(errno);
        event_callback(netlib_event(BOOST_ERROR,err));
    }

    if(perform_free){
        buffer_cache.return_buffer(buf);
    }
}

void CommLink::do_recv()
{
    socket_.async_receive_from( boost::asio::buffer(input_buffer),
            sender,
            bind(&CommLink::read_handler,this,_1,_2)
            );
}

size_t data_packet::header_size()
{
    return sizeof(data_packet);

}
void CommLink::handle_data(const vector<char>& data, size_t size)
{
    data_packet* p  = (data_packet *) data.data();  
    size_t header_size = data_packet::header_size();
    size_t data_len = size - header_size;
    //if packet contains a header
    if(size >= header_size){
        acknowledge_packet(p->seq_num);
        //if thats a packet we're waiting for 
        if(p->seq_num == conseq_recv ){
            //send data to user
            recv_callback(p->data,data_len);        
            conseq_recv++;
            //return all other consecutive packets
            while(incoming_package_storage.begin()->first == conseq_recv && !incoming_package_storage.empty()){
                shared_ptr<vector<char>> v;
                tie(v,data_len) = incoming_package_storage.begin()->second; 
                //send data to user
                incoming_package_storage.erase(incoming_package_storage.begin()); 
                conseq_recv++;
                buffer_cache.return_buffer(v);
            }

            //if it's some newer packet
        }else if(p->seq_num > conseq_recv ){
            //cache it
            auto buf = buffer_cache.get_buffer(data_len);
            copy(p->data,p->data+data_len,buf->begin());
            incoming_package_storage[p->seq_num] = make_tuple(buf,data_len);
        }
    }
}

void CommLink::handle_ack(std::vector<char>& data)
{



    ack_packet *ack = (ack_packet*)data.data();
    auto packet_data = unconfirmed_packets.find(ack->ack_num);


    if(packet_data!=unconfirmed_packets.end()){
        shared_ptr<vector<char>> b;
        uint32_t time_left;
        uint32_t data_len;
        tie(b,data_len,time_left) = packet_data->second; 
        unconfirmed_packets.erase(packet_data);
        buffer_cache.return_buffer(b);
    }



}
void CommLink::read_handler(const boost::system::error_code& err, std::size_t transferred)
{


    if(!err){

        if( sender == endpoint && transferred >= sizeof(packet) ){

            ts_last_packet=ms_since_epoch();
            packet* p = (packet *) input_buffer.data();

            switch(p->type){
                case DATA:
                    handle_data(input_buffer,transferred);
                    break;

                case ACK:

                    if(transferred>=sizeof(ack_packet)){
                        handle_ack(input_buffer);
                    }

                    break;
                case PING:

                    if(transferred>=sizeof(ping_packet)){
                        respond_ping(input_buffer,transferred);
                    }

                    break;
                case PING_RESPONSE:
                    if(transferred>=sizeof(ping_packet)){
                        process_ping(input_buffer);
                    }
                    break;
                case DISCONNECT:
                    handle_dc();
                    break;
                default:
                    //random crap, drop it
                    break; 
            };

        }

        do_recv();
    }else if(err != boost::asio::error::operation_aborted ){
        BOOST_LOG_TRIVIAL(error)<<"error @read_handler: "<<err.message();
        event_callback(netlib_event(BOOST_ERROR,err));
    }


}

void CommLinkClient::await_connection(const boost::system::error_code& err, size_t size){

    if(!err){
        ping_packet *p = (ping_packet *) input_buffer.data();

        if(size>=sizeof(ping_packet) && p->type == PING && sender == endpoint ){
            unique_lock<mutex> conlock(connection_lock);
            start_comm_protocol();
            conlock.unlock();
            awaiting_connection.notify_one();

        }else{
            socket_.async_receive_from( boost::asio::buffer(input_buffer),
            sender,
            bind(&CommLinkClient::await_connection,this,placeholders::_1,placeholders::_2)
            );
        }
    
    }else if(err != boost::asio::error::operation_aborted){
        BOOST_LOG_TRIVIAL(error)<<"error @CommLinkClient::await_connection: "<<err.message();
        event_callback(netlib_event(BOOST_ERROR,err));
    }

}

void CommLinkServer::await_connection(const boost::system::error_code& err, size_t size)
{


    if(!err){

        ping_packet *p = (ping_packet *) input_buffer.data();
        if(size>=sizeof(ping_packet) && p->type == PING){
            endpoint = sender;
            send_ping_packet();
            start_comm_protocol();          
        }else{
            socket_.async_receive_from( boost::asio::buffer(input_buffer),
            sender,
            bind(&CommLinkServer::await_connection, this, placeholders::_1 ,placeholders::_2)
            );
        } 
    
    }else if(err != boost::asio::error::operation_aborted){
        BOOST_LOG_TRIVIAL(error)<<"error @CommLinkServer::await_connection "<<err.message();
        event_callback(netlib_event(BOOST_ERROR,err));
    }
   
}


//we're waiting for a ping packet, when we receive it we say we have estabilished a connection a respond 
//the client with ping packet of our own
CommLinkServer::CommLinkServer(std::shared_ptr<boost::asio::io_service>& io_service, short port,uint64_t _update_interval): 
    CommLink(io_service, boost::asio::ip::udp::endpoint(
        boost::asio::ip::udp::v4(), port),_update_interval)
{

    io->post(
        [=]()
        {
            socket_.async_receive_from( boost::asio::buffer(input_buffer),
                sender,
                bind(&CommLinkServer::await_connection,this,placeholders::_1,placeholders::_2)
            );
        
        
        }         
            
    );

};
