#pragma once
#include "config.h"
#include <vector>
#include <memory>
#include <mutex>
#include <chrono>
#include <functional>
#include <iostream>
#include <type_traits>
#include <boost/asio.hpp>

#include <cassert>

uint64_t ms_since_epoch();

template<typename T>
class BufferCache{
    private:
        std::vector<std::shared_ptr<std::vector<T>>> cache;
        uint32_t _buffer_size;
    public:
        BufferCache(uint32_t cache_size,uint32_t buffer_size):_buffer_size(buffer_size){
            cache.resize(cache_size);
            for(auto& buf_ptr: cache){
                buf_ptr = std::make_shared<std::vector<T>>(buffer_size);
            }
        };
        std::shared_ptr<std::vector<T>> get_buffer(uint32_t size){
            if(cache.empty()){
                return std::make_shared<std::vector<T>>(std::max(size,_buffer_size)); 
            }else{
                auto buf = cache.back();
                if(buf->size() < size){
                    buf->resize(size); 
                }
                cache.pop_back();
                assert(buf);
                return buf;
            }
        };
        void return_buffer(std::shared_ptr<std::vector<T>> buf){if(buf)cache.push_back(buf);};
        void alloc_additional(uint32_t count){
            unsigned int previous_size = cache.size();
            cache.resize(cache.size()+count);
            //add to the end of cache some new buffers
            for(unsigned int i = previous_size;i<cache.size();i++ ){
                cache[i]=std::make_shared<std::vector<T>>(_buffer_size); 
            }
        };

};

//TODO: clear buffer after receiving
//thread safe version of buffer cache
template <typename T>
class BufferCache_safe{
    private:
        std::mutex cache_lock;
        BufferCache<T> cache;
    public:
        BufferCache_safe(uint32_t cache_size,uint32_t buffer_size): cache(cache_size,buffer_size){};
        inline std::shared_ptr<std::vector<T>> get_buffer(uint32_t size){std::lock_guard<std::mutex> l(cache_lock); return cache.get_buffer(size);};
        inline void return_buffer(std::shared_ptr<std::vector<T>> buf){std::lock_guard<std::mutex> l(cache_lock);cache.return_buffer(buf);};
        inline void alloc_additional(uint32_t count, uint32_t buffer_size){
            std::lock_guard<std::mutex> l(cache_lock);
            cache.alloc_additional(count,buffer_size); 
        };

};



class PeriodicCaller{
    private:
        boost::asio::deadline_timer period_timer;
        std::vector<std::function<void()>> callbacks;
        uint32_t period;
    public:
        PeriodicCaller(boost::asio::io_service& io, uint32_t _period) : period_timer(io), period(_period){
            period_timer.expires_from_now(boost::posix_time::milliseconds(_period));
            period_timer.async_wait(std::bind(&PeriodicCaller::callback_runner,this,std::placeholders::_1));
        };
        void add_callback(std::function<void()> callback){callbacks.push_back(callback);};
        void callback_runner(const boost::system::error_code& ec);



};


template<typename T>class  safe_function{};

template<typename Res, typename... Args>
class safe_function<Res(Args...)>{
    private:
        std::function<Res(Args...)> func,tmp;

        bool swapped = false;
    public:
        safe_function(){};
        safe_function(typename std::common_type<std::function<Res(Args...)>>::type f){
            func = f;
        };

        safe_function& operator=( typename std::common_type<std::function<Res(Args...)>>::type f){
            tmp = f;
            swapped=true;
            return *this;
        };
        Res  operator()(Args... args){
            if(swapped){
                func = tmp;
                swapped=false;
            }
            return func(args...);
        }


};
