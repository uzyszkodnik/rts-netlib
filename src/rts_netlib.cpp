#include "hiddenconf.h"
#include "rts_netlib.h"
#include "comm_protocol.h"
#include <boost/asio.hpp>
#include <iostream>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <thread>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

namespace logging = boost::log;

using namespace std;
using namespace boost::asio::ip;



void PauseActivitiesTracker::update(){

    //TODO: refactor this with update_pause eh
    for(auto i = activities.begin(); i!= activities.end();){
        uint32_t& time_left = get<0>(i->second);
        if(time_left < update_time){
            pausing[i->first]=false;
            auto f= get<1>(i->second); 
            activities.erase(i++);
            f();
        }else{
            time_left -= update_time; 
            i++;
        }
    }
    update_pause();

}

void PauseActivitiesTracker::add_activity(uint32_t player_id,std::function<void()> f){
    activities[player_id] = make_tuple(player_times[player_id],f);
    pausing[player_id]=true;
}

void PauseActivitiesTracker::update_pause(){
    for(uint32_t i = 0 ; i < player_times.size();i++){
        if(pausing[i]){
            if(player_times[i]<update_time){
                player_times[i]=0;    
            }else{
                player_times[i]-=update_time; 
            }
        }else{
            player_times[i]=min(player_times[i]+update_time, max_time);
        }
    }  

}

TurnPaceTracker::TurnPaceTracker(uint32_t turn_len, uint32_t _maximal_slowdown, uint32_t player_count):
    original_turn_len(turn_len)
    ,current_turn_len(turn_len)
    ,maximal_slowdown(_maximal_slowdown)
    ,pace_requests(player_count, turn_len) {}


void TurnPaceTracker::update_turn_len(){
   auto maximal = max_element(pace_requests.begin(),pace_requests.end()); 
   current_turn_len = *maximal;
}

void TurnPaceTracker::remove_client(uint32_t player_id){
    pace_requests[player_id]=original_turn_len;
    update_turn_len();    
}

bool TurnPaceTracker::add_slowdown(uint32_t player_id, uint32_t value){
    if(value > maximal_slowdown){
        return false ;
    }else{
        pace_requests[player_id] = min( max(value,original_turn_len), maximal_slowdown);
        update_turn_len();
        return true;
    }
}


Turn::Turn(BufferCache<char> &c, uint32_t turn_id) : cache(c){
    assert(sizeof(turn_header_pkg) < mtu);

    auto b = c.get_buffer(mtu);

    header  = (turn_header_pkg *) b->data();
    header->type = TURN_HEADER;
    header->followup_count = 0 ;
    header->turn_id = turn_id;
    header->cmd_count =0 ;

    current_fragment = &header->data_fragment;
    current_fragment->command_count=0; 
    current_fragment->type=TURN_FRAGMENT;

    write_pos = (char *)current_fragment->commands;
    write_end = b->data()+mtu;
    pkg_chain.push_back(b);

}

void Turn::set_expected_time(uint32_t time){
    header->schedule_time=time;
}

void Turn::write_command(uint32_t player_id, const char* data, uint32_t len){

        uint32_t command_len = sizeof(command)+len; 
        command* cmd =(command*) write_pos;
        cmd->size=len;
        cmd->player_id=player_id;
        copy(data,data+len, cmd->data);

        write_pos+=command_len;

        current_fragment->command_count++;
        header->cmd_count++;

}

void Turn::add_command(uint32_t player_id, const char* data,uint32_t len){

    if(len == 0 ) 
        return;

    //if current buffer can store additional command
    //clang treats char data[] as of size 0
    assert(len <= max_cmd_size); 
    uint32_t command_len = sizeof(command)+len; 
    //if we've got no space in current buffer alloc new one
    if(write_pos + command_len > write_end){
        auto b = cache.get_buffer(mtu);
        current_fragment =(turn_fragment *) b->data();
        current_fragment->type = TURN_FRAGMENT;
        current_fragment->command_count=0;

        write_pos = (char*)current_fragment->commands;
        write_end = b->data()+mtu;

        pkg_chain.push_back(b);

        header->followup_count++;
    }

    write_command(player_id,data,len);

}

void TurnUnpacker::process_header(const char *data, uint32_t size){
    //not valid pkg => dont process this turn
    if(size < sizeof(turn_header_pkg)){
        waiting_for_header = true;
        return; 
    }

    turn_header_pkg* p = (turn_header_pkg *) data;
    time = p->schedule_time;
    followup_count = p->followup_count;
    turn_id = p->turn_id;

#ifdef RTS_NETLIB_DEBUG
    crc = p->crc;
#endif

    waiting_for_header = false;

    packets = cmd_cache.get_buffer(p->cmd_count);
    packets->clear();

    uint32_t rest_size = size - (sizeof(turn_header_pkg) - sizeof(turn_fragment)); 

    if(rest_size >= sizeof(turn_fragment)){
        process_fragment((char*)&p->data_fragment,rest_size);
    }else{
        clear();
    }

}

void TurnUnpacker::process_fragment(const char* data, uint32_t size){
    if(waiting_for_header)
        return;

    turn_fragment* p = (turn_fragment *) data;
    uint32_t commands_in_packet = p->command_count;

    const char* end = data+size;
    char* position = (char *)p->commands;        
    for(uint32_t i = 0; i < commands_in_packet;i++){
        command* cmd = (command *) position;

        if( position == end || end < position + sizeof(command) ){
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"@process_fragment - cmd not in buffer";
#endif

            clear();
            return;
        }

        uint32_t cmd_size = sizeof(command)+cmd->size;

        auto cmd_end = position+cmd_size;
        //cmd is pointing somewhere out of the packet size, generally wtf
        //drop it
        if(end < cmd_end){
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"@process_fragment - some weird shit received";
#endif

#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"diff "<<uint32_t(cmd_end-end)<<endl;
#endif

            clear();
            return;
        }

        auto buf = cache.get_buffer(cmd->size);
        copy(cmd->data,cmd->data+cmd->size,buf->data());
        packets->push_back(turn_command(cmd->player_id,cmd->size,buf));
        position=cmd_end; 

    }

}

bool TurnUnpacker::process_packet(const char* data, uint32_t size){
    netlib_msg* p =(netlib_msg *) data; 

    if(p->type == TURN_HEADER && size >= sizeof(turn_header_pkg)){
        process_header(data,size); 
    }else if(p->type == TURN_FRAGMENT && size >= sizeof(turn_header_pkg)){
        process_fragment(data,size); 
        followup_count--;
        //something unexpected
    }else{
#ifdef RTS_NETLIB_DEBUG
        BOOST_LOG_TRIVIAL(debug)<<"@process_packet some unexpected crap";
#endif

        clear();
    }

    //last packet in turn
    if(followup_count == 0 && !waiting_for_header) {
        return true; 
    }else{
        return false; 
    }

}

//cleanup when we encounter sth unexpected
void TurnUnpacker::clear(){
    for(auto& i : *packets){
        cache.return_buffer(i.command);
    }
    packets->clear();
    cmd_cache.return_buffer(packets);
    waiting_for_header=true;
}

RtsClient::RtsClient(std::shared_ptr<boost::asio::io_service>& _io_service, uint32_t _id, uint64_t _password):
    password(_password)
    ,io_service(_io_service)
    ,id(_id)
{
    recv_callback =  [](const char*,uint32_t){};
    event_callback = [](const netlib_event&){};
};

void RtsClient::disconnect_quietly(){
    mutex dc_lock;
    unique_lock<mutex> l(dc_lock);
    condition_variable dc_cond;
    io_service->post([&,this]{
            unique_lock<mutex> l2(dc_lock);
            client.reset();
            connected=false;
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"signaling client bout release";
#endif

            l2.unlock();
            dc_cond.notify_one();
            });

#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"waiting for free";
#endif

    dc_cond.wait(l,[this]{return !connected;});
    assert(!client);
#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"freed";
#endif

}

void RtsSimClient::recv_handler(const char* data,uint32_t size){
#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"recv handler"<<id;
#endif

    if(size >= sizeof(netlib_msg)){
        netlib_msg* p = (netlib_msg*) data; 

        if(p->type == TURN_HEADER || p->type == TURN_FRAGMENT){
            //process packet
            //if thats the last for this turn
            //copy the turn to the queue and notify the client
            if(unpacker.process_packet(data,size)){
                unique_lock<mutex> l(sim_lock);
                auto cmds = unpacker.get_cmds();

                uint64_t client_sched_time = ms_since_epoch() + (uint64_t(unpacker.get_schedule_time()) - uint64_t(client->get_ping()));
#ifdef RTS_NETLIB_DEBUG
                turn_commands.push_back(make_tuple(client_sched_time,cmds,unpacker.checksum()));

#else
                turn_commands.push_back(make_tuple(client_sched_time,cmds));
#endif

                l.unlock();
                turn_ready.notify_one();
            } 

        } 
        if(p->type == SERVER_TURN && size >= sizeof(server_turn_info)){
            server_turn_info* t = (server_turn_info*) data; 
            server_turn=t->turn;
        }

        if(p->type == SERVER_TURN_LEN && size >= sizeof(server_turn_len)){
            server_turn_len* p = (server_turn_len *) data;
            original_turn_len = p->turn_len; 
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"received turn len"<<original_turn_len;
#endif

        }
        if(p->type == AUTH_FAILED){
            event_callback(netlib_event(FAILED_AUTH));
        }
    }


}

void RtsSimClient::free_cmds(std::shared_ptr<std::vector<turn_command>> cmds ){

    for(auto& i: *cmds){
        buffer_cache.return_buffer(i.command);
    }
    cmds->clear();
    cmd_cache.return_buffer(cmds);
}


std::shared_ptr<std::vector<turn_command>> RtsSimClient::receive_commands(){
    std::unique_lock<mutex> l(sim_lock);


    if (event_occured) {
        event_occured = false;
        return shared_ptr<vector<turn_command>>(nullptr);
    }

    uint32_t turn_time = sim_time.get_tss();

    //if we were slowing down the pace check whether we are doing it faster
    //if so change the pace
    if(client_state == SLOWED){
        //we're processing at normal speed
        if(turn_time <= original_turn_len){
            client_state = NORMAL;
            sim_pace_request slowdown(turn_time);
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"slowdown request - processing @normal speed";
#endif

            client->send_data( (const char*) &slowdown, sizeof(slowdown));
            //faster than our previous slowdown       
        }else if (turn_time+10 < current_slowdown){
            current_slowdown = turn_time+10;     
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"slowdown request - processing faster";
#endif

            sim_pace_request slowdown(current_slowdown);
            client->send_data( (const char*) &slowdown, sizeof(slowdown));
        } 
    } 

    //no new cmds to process
    //wait for some new commands
    //update state if needed
    if(turn_commands.size() == 0 ){
        //if we were catching up  or replaying determine our lag and inform server we are ready to go
        if(client_state == CATCHING_UP || (client_state==REPLAYING && current_turn == server_turn)){

#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"we were catching up current turn len is: "<<original_turn_len;
#endif

            client_state = NORMAL;

            //if last turn took longer to process than the original turn len
            if(turn_time > original_turn_len){
#ifdef RTS_NETLIB_DEBUG
                BOOST_LOG_TRIVIAL(debug)<<"slowdown request - sim too slow, finished catching up";
#endif

                current_slowdown = turn_time+10;
                sim_pace_request slowdown(current_slowdown); 
                client->send_data((const char*)&slowdown, sizeof(slowdown));
                client_state = SLOWED;
            }

            //unpause the sim
            netlib_msg start_req(START_REQUEST); 
            client->send_data((const char*)&start_req, sizeof(start_req));

        } 
        //wait for some commands to arrive
        turn_ready.wait(l,[=](){return turn_commands.size() > 0 || event_occured ;});
    }

    if (event_occured) {
        event_occured = false;
        return shared_ptr<vector<turn_command>>(nullptr);
    }

    //we are guaranteed that we will have commands here
    assert(turn_commands.size() > 0 ) ;

    uint64_t sched_time;
    std::shared_ptr<vector<turn_command>> cmds;

    if(turn_commands.size()>0){
        sched_time = get<0>(turn_commands.front()) ;
        cmds = get<1>(turn_commands.front());
    }else{
        //we've received a signal to wake up
        sched_time = ms_since_epoch()+10 ; 
        cmds = make_shared<vector<turn_command>>(0); 
    }


#ifdef RTS_NETLIB_DEBUG
    uint32_t checksum = get<2>(turn_commands.front());
#endif

    turn_commands.pop_front();

    //unlock since we dont need to interact with the turn_commands anymore
    l.unlock();

    uint64_t current_time = ms_since_epoch();

    //we're behind 
    if(sched_time < current_time){
        //we're behind and we didnt knew about it
        //so lets start catching up
        if(client_state == SLOWED || client_state == NORMAL){
            client_state = CATCHING_UP; 
            netlib_msg stop_req(STOP_REQUEST);
            client->send_data((const char*)&stop_req,sizeof(stop_req));  
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"we're behind, catching up";
#endif

        } 
        //if we know about it simply skip the if and return the data
        //no additional magic is needed since the server knows about it aswell and is not sending new pkgs

        //turn is scheduled for some time from now so lets wait
    }else{
        this_thread::sleep_for(chrono::milliseconds(sched_time - current_time)); 
    }

#ifdef RTS_NETLIB_DEBUG
    for(auto& cmd: *cmds){
        crc.process_bytes(cmd.command->data(), cmd.size); 
    }
    crc.process_bytes((char*)&current_turn,sizeof(current_turn));
    assert(checksum == crc.checksum() );
#endif

    current_turn++;

    sim_time.start();
    return cmds;

}

void RtsClient::await_connection(const char* data , uint32_t len){
    if(len >=sizeof(netlib_msg)){

        netlib_msg* m = (netlib_msg*) data;

        unique_lock<mutex> l(connection_lock);
        switch(m->type){
            case AUTH_FAILED:
                connected=false;
                auth_performed=true;
                l.unlock();

                event_callback(netlib_event(FAILED_AUTH));
                break;
            case AUTH_SUCCESS: 
                auth_performed=true;
                connected=true;
                l.unlock();
                break;
            default:
                break;
        }

    }

    if(auth_performed){
        client->set_recv_callback(recv_callback);
        awaiting_connection.notify_one(); 
    }

}

bool RtsClient::connect(const std::string& _ip, const std::string& _port){
    auth_performed=false;
    connected=false;

    port = _port;
    ip = _ip;

    //fetch port number for udp connection
    tcp::socket s(*io_service);
    tcp::resolver resolver(*io_service);
    boost::asio::connect(s,resolver.resolve({ip,port}));
    uint16_t udp_port;
    boost::asio::read(s,boost::asio::buffer((char *)&udp_port,sizeof(udp_port)));
    //connect to udp server
    client.reset(new CommLinkClient(io_service));

    if(! client->connect(ip, to_string(udp_port))){
        return false; 
    }

    client->set_event_callback([this](const netlib_event& event){
            if(event == CONNECTING_TIMED_OUT || event == DISCONNECTED || event == BOOST_ERROR ){
                connected = false; 
            }
            event_callback(event);
            });

    //authenticate
    connection_info credentials;
    credentials.client_type = type;
    credentials.id = id;
    credentials.password=password;
    credentials.turn = current_turn;

    client->set_recv_callback(bind(&RtsClient::await_connection,this,placeholders::_1,placeholders::_2));
#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"current turn: "<<current_turn;
#endif


    unique_lock<mutex> l(connection_lock);
    client->send_data((char *) &credentials, sizeof(credentials));
    awaiting_connection.wait_for(l,chrono::seconds(5), [=]{return auth_performed;});

    return connected;
}

bool RtsServer::client_is_connected(uint32_t client_id){
    return clients[client_id][UI_CLIENT] && clients[client_id][SIM_CLIENT];
}


void RtsServer::remove_client(uint32_t client_id){
#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"removing client: "<<client_id;
#endif

    auto& v = clients[client_id];
    v[SIM_CLIENT]=nullptr;
    v[UI_CLIENT]=nullptr;

    game_pace.remove_client(client_id);

    player_drop_msg dropped(client_id); 
    send_to_connected_clients((const char*)&dropped, sizeof(dropped),UI_CLIENT);
    send_to_connected_clients((const char*)&dropped , sizeof(dropped), SIM_CLIENT);
    banned[client_id]=true;
}

void RtsServer::continue_simulation(){
    turn_timer.expires_from_now(boost::posix_time::milliseconds(game_pace.get_turn_len()));
    turn_timer.async_wait(bind(&RtsServer::turn_callback,this,placeholders::_1));

    set_sim_data_callbacks();

    netlib_msg resumed(SIMULATION_RESUMED);
    send_to_connected_clients((const char*)&resumed, sizeof(resumed), UI_CLIENT);
}

void RtsServer::dead_sockets_cleanup(){
    dead_sockets.clear();  
    auto i = pending_connections.begin();

    while(i != pending_connections.end()){
        uint32_t& time_left = get<0>(i->second);
        if(time_left<cleanup_interval){
            //removing from map doesn't invalidate other iterators
            pending_connections.erase(i++); 
        }else{
            time_left-=cleanup_interval;
            i++;
        }

    }

}

bool RtsServer::auth_client(connection_info* credentials){
    if(credentials->id < passwords.size() 
            && passwords[credentials->id]==credentials->password 
            && credentials->client_type<2 
            && !clients[credentials->id][credentials->client_type] 
            && !banned[credentials->id]
            && credentials->turn  <= turn_id){
        return true;
    }
    return false;
}

bool RtsSimClient::connect(const string& ip,const string& port){
    auto ret = RtsClient::connect(ip,port);
    sim_time.start();
    return ret;
}

void RtsSimClient::netevent_callback(const netlib_event& ev){
    unique_lock<mutex> l(sim_lock);
    last_msg = ev;
    event_occured=true;
    l.unlock();

    turn_ready.notify_one();
}

bool RtsSimClient::reconnect(){
    client_state = REPLAYING;
    auto ret = RtsClient::reconnect(); 
    sim_time.start();
    return ret;
}

void RtsServer::auth_udp(uint64_t cache_id, const char* data, uint32_t len){

    bool auth_failed = true;
    //if we are in this callback the class we are trying to retrieve from cache
    //must not have been deleted thus no need for checking pointer validity
    //also this callback occures only once
    auto udp_server=get<1>(pending_connections[cache_id]);
    assert(udp_server.get() != nullptr);
    if(len>=sizeof(connection_info)){
        //we will only listen to only one authentication - this one
        BOOST_LOG_TRIVIAL(info)<<"client connected "; 
        udp_server->set_recv_callback_unsafe([](const char*,uint32_t){});
        connection_info* credentials = (connection_info*) data; 

        if(auth_client(credentials)){
#ifdef RTS_NETLIB_DEBUG
            BOOST_LOG_TRIVIAL(debug)<<"client authed id:"<<credentials->id<<" type:"<<credentials->client_type;
#endif

            auth_failed =false;
            clients[credentials->id][credentials->client_type]=udp_server;

            netlib_msg auth_success(AUTH_SUCCESS);
            udp_server->send_data_unsafe((char*)&auth_success,sizeof(auth_success));

            if(credentials->client_type == SIM_CLIENT){
                udp_server->set_recv_callback_unsafe(
                        bind(sim_callback,credentials->id, placeholders::_1,placeholders::_2));
                udp_server->set_event_callback_unsafe(bind(dc_callback,placeholders::_1,credentials->id,SIM_CLIENT));

                //send the turn len
                server_turn_len turn_len_pkg(turn_len);
                udp_server->send_data_unsafe((const char*) &turn_len_pkg,sizeof(turn_len_pkg));
                current_player_turns[credentials->id]=credentials->turn;

            }else{
                udp_server->set_recv_callback_unsafe(
                        bind(ui_callback,credentials->id,placeholders::_1,placeholders::_2) 
                        );
                udp_server->set_event_callback_unsafe(bind(dc_callback,placeholders::_1,credentials->id,UI_CLIENT));
            }


            if(client_is_connected(credentials->id)){
                connected_clients++; 
                //we're paused => player is reconnecting
                //send him missed packets 
                //he has to process them during the pause time otherwise
                //he will be kicked and banned
                if(server_state == PAUSED ){
                    handle_replay_request(credentials->id,current_player_turns[credentials->id]);
                }

            } 

            if(connected_clients == passwords.size() && server_state==AWAITING_START){
                start_simulation(); 
            }


            //if all of the expected clients have connected uis and sims start the simulation

            //let cleanup remove it t&& inform it that auth has failed
        } 
    }

    if(auth_failed){
#ifdef RTS_NETLIB_DEBUG
        BOOST_LOG_TRIVIAL(debug)<<"auth failed "; 
#endif


        //inform the client that auth failed
        //generally that means that we are banned  
        netlib_msg auth_fail(AUTH_FAILED);
        udp_server->send_data_unsafe((const char*)&auth_fail,sizeof(auth_fail));

    }
} 

void RtsServer::accept_handler(const boost::system::error_code& ec){
    if(!ec){
        auto server = make_shared<CommLinkServer>(io_service);

        uint16_t udp_port = server->get_port();

        //register socket for removal in 2 seconds if client doesnt connect
        //or improperly authenticates
        auto cache_id = pending_id++;
        pending_connections[cache_id] = make_tuple(2000,server);

        //authenticate the client
        server->set_recv_callback_unsafe( bind(&RtsServer::auth_udp,this, cache_id,placeholders::_1, placeholders::_2));

        try{
            //redirect incoming connection to udp server
            boost::asio::write(s,boost::asio::buffer((char *)&udp_port,sizeof(udp_port)));
            s.close();

        }catch(boost::system::system_error& e){
            BOOST_LOG_TRIVIAL(error)<<"error @ accept_handler write: "<<e.code()<<" "<<strerror(errno);
        }

        try{
            a.async_accept(s,bind(&RtsServer::accept_handler,this,placeholders::_1));
        }catch(boost::system::system_error& e){
            BOOST_LOG_TRIVIAL(error)<<"error @ accept_handler write: "<<e.code()<<" "<<strerror(errno);
        }

    }else if(ec != boost::asio::error::operation_aborted){
        BOOST_LOG_TRIVIAL(error)<<"error @ accept handler: "<<ec.message();
    }



}



void RtsServer::pause_handler(){
    assert(server_state==PAUSED);

    if( pause_activities.pausing_clients() ==  0 ){

        if(connected_clients == 0 ){
            event_callback(netlib_event(SIMULATION_FINISHED)); 
        }else{
            run_simulation(); 
        } 

    }

}


RtsServer::RtsServer(std::shared_ptr<boost::asio::io_service>& _io_service,const std::string& port,const std::vector<uint64_t>& _passwords,uint32_t _turn_ms_len,uint32_t max_slowdown, uint32_t _session_len,uint32_t _pkg_size_bound):

    cache(0, mtu)
    ,io_service(_io_service)
    ,banned(_passwords.size(),false)
    ,s(*_io_service)
,a(*_io_service 
        ,boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(),std::stoi(port)))
    ,passwords(_passwords)
    ,turn_timer(*_io_service)
    ,session_len(_session_len)
    ,pkg_size_bound(_pkg_size_bound)
    ,game_pace(_turn_ms_len, max_slowdown,_passwords.size())
    ,periodic_tasks(*io_service,periodic_update_interval)
    ,pause_activities(*io_service,60000,_passwords.size(), 1000)
    ,turn_len(_turn_ms_len)
,current_player_turns(_passwords.size(),0)

{
    clients.resize(passwords.size());
    for(auto& i  : clients){
        i.resize(2); 
    }
    double avg_turn_packet_size = double(sizeof(turn_header_pkg) + passwords.size()*(pkg_size_bound+sizeof(command)));
    uint32_t turns_per_game = (uint32_t)ceil(1000.0f/((float)_turn_ms_len)*60.0f*((float)_session_len)*2.0f);
    uint32_t packets_per_turn = ceil(avg_turn_packet_size/double(mtu));
    uint32_t buffers_needed = packets_per_turn*turns_per_game;
#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"turns_per_game "<<turns_per_game<<" avg_size: "<<avg_turn_packet_size<<" packets per turn: "<<packets_per_turn<<" buffers needed: "<<buffers_needed;
#endif

    cache.alloc_additional(buffers_needed);
    dc_callback = bind(&RtsServer::connection_phase_dc,this,placeholders::_1,placeholders::_2,placeholders::_3);

    periodic_tasks.add_callback(bind(&RtsServer::dead_sockets_cleanup,this));
}


void RtsServer::connection_phase_dc(const netlib_event& event,uint32_t client_id, uint32_t type){

    if( event == DISCONNECTED  && client_is_connected(client_id)){
        connected_clients--;
    }

    shared_ptr<CommLinkServer> p(nullptr);
    swap(p, clients[client_id][type]);
    dead_sockets.push_back(p);

}

void RtsServer::ui_recv_handler(uint32_t player_id, const char* data, uint32_t len){

#ifdef RTS_NETLIB_DEBUG
    turn_crc.process_bytes(data,len);
    BOOST_LOG_TRIVIAL(debug)<<player_id<<" len "<<len<<" turn "<<turn_id;
#endif

    auto b =  cache.get_buffer(len);  
    copy(data,data+len,b->begin());
    current_turn->add_command(player_id,data,len);

}

void RtsServer::pause(uint32_t player_id){
    if(server_state!=PAUSED){
        server_state = PAUSED;
        turn_timer.cancel();
    }

    pause_activities.add_activity(player_id,[=](){
            remove_client(player_id); 
            pause_handler();
            }); 

}

void RtsServer::sim_recv_handler(uint32_t player_id, const char* data,uint32_t len){

    if(len >= sizeof(netlib_msg)){
        netlib_msg* p  = (netlib_msg*) data;
        switch(p->type){
            case PACE_REQUEST:
                if(len >= sizeof(sim_pace_request)){
                    sim_pace_request* req = (sim_pace_request *)data;
#ifdef RTS_NETLIB_DEBUG
                    BOOST_LOG_TRIVIAL(debug)<<"game pace request from: "<<player_id<<"time: "<<req->turn_time;
#endif

                    if(game_pace.add_slowdown(player_id, req->turn_time)){
#ifdef RTS_NETLIB_DEBUG
                        BOOST_LOG_TRIVIAL(debug)<<"request accepted"; 
#endif

                    }else{
#ifdef RTS_NETLIB_DEBUG
                        BOOST_LOG_TRIVIAL(debug)<<"request dropped";
#endif

                        remove_client(player_id); 
                    }
                }
                break;
            case STOP_REQUEST:
#ifdef RTS_NETLIB_DEBUG
                BOOST_LOG_TRIVIAL(debug)<<"stop request from: "<<player_id;
#endif


                pause(player_id);
                break;
            case START_REQUEST:
#ifdef RTS_NETLIB_DEBUG
                BOOST_LOG_TRIVIAL(debug)<<"start request from: "<<player_id;
#endif

                pause_activities.remove_activity(player_id);
                pause_handler();
                break;

            default:
                break; 

        }
    }  


}


uint64_t RtsServer::get_max_ping(CLIENT_TYPE type){
    uint64_t max_ping = 0 ;
    for(const auto& client: clients){
        auto c = client[type];
        if(c){
            max_ping = max(max_ping,c->get_ping());
        }

    }
    return max_ping;

}

void RtsServer::send_to_connected_clients(const char* data,uint32_t len, CLIENT_TYPE type){
    for(const auto& client: clients){
        auto& s = client[type];
        if(s){
            s->send_data_unsafe(data,len); 
        } 
    }
}

void RtsServer::turn_callback(const boost::system::error_code& ec){
    if(!ec){
        turn_timer.expires_from_now(boost::posix_time::milliseconds(game_pace.get_turn_len()));
        turn_timer.async_wait(bind(&RtsServer::turn_callback,this,placeholders::_1));

        //hopefully it will discard higher order bits
        //schedule turn for processing in max_ping+10 miliseconds ( in case of some minor lag spikes )
        uint32_t turn_delay = (uint32_t) get_max_ping(SIM_CLIENT)+10;
        current_turn->set_expected_time(turn_delay);

#ifdef RTS_NETLIB_DEBUG
        turn_crc.process_bytes((char*)&turn_id, sizeof(turn_id));
        current_turn->add_crc(turn_crc.checksum());
#endif

        const auto& l = current_turn->get_turn_pkgs();
        for(const auto& pkg :  l){
            send_to_connected_clients(pkg->data(),pkg->size(),SIM_CLIENT);
        }

        //order of nanoseconds i hope eh
        //TODO:optimize
        past_turns.push_back(current_turn);
        current_turn = make_shared<Turn>(cache,++turn_id);
    }else if(ec != boost::asio::error::operation_aborted){
        BOOST_LOG_TRIVIAL(error)<<"error @turn callback"<<ec.message();
    }

}

void RtsServer::set_sim_data_callbacks(){
    dc_callback = bind(&RtsServer::client_dc_callback,this,placeholders::_1, placeholders::_2,placeholders::_3);
    ui_callback = bind(&RtsServer::ui_recv_handler,this,placeholders::_1,placeholders::_2,placeholders::_3);
    sim_callback = bind(&RtsServer::sim_recv_handler,this,placeholders::_1,placeholders::_2,placeholders::_3);

    for(uint32_t client_id =  0 ;  client_id < clients.size();client_id++){
        auto& client = clients[client_id]; 
        auto sim = client[SIM_CLIENT];
        auto ui = client[UI_CLIENT];

        // generally we shouldnt set callbacks for someone with unconnected ui or sim right?
        // and such situation shouldnt happen at all when starting the sim
        assert( sim&&ui );
        if(sim && ui ) { 
            sim->set_recv_callback_unsafe(
                    bind(&RtsServer::sim_recv_handler,this,client_id,placeholders::_1, placeholders::_2)
                    );

            ui->set_recv_callback_unsafe(
                    bind(&RtsServer::ui_recv_handler,this,client_id,placeholders::_1, placeholders::_2)        
                    );

            sim->set_event_callback_unsafe(
                    bind(&RtsServer::client_dc_callback,this,placeholders::_1,client_id,SIM_CLIENT)
                    );

            ui->set_event_callback_unsafe(
                    bind(&RtsServer::client_dc_callback,this,placeholders::_1,client_id,UI_CLIENT)
                    );

        }
    }

}

void RtsServer::client_dc_callback(const netlib_event& event, uint32_t client_id, uint32_t type){
#ifdef RTS_NETLIB_DEBUG
    BOOST_LOG_TRIVIAL(debug)<<"client with id: "<<client_id<<" disconnected";
#endif

    if(event != DISCONNECTED){
        event_callback(event);
        return; 
    }

    pause(client_id); 

    connected_clients--;

    //remove both sockets since both probably dcd
    shared_ptr<CommLinkServer> sim(nullptr);
    shared_ptr<CommLinkServer> ui(nullptr);
    swap(sim ,clients[client_id][SIM_CLIENT]);
    swap(ui, clients[client_id][UI_CLIENT]);

    //if some of them can fire a callback dont let them to
    if(sim){
        sim->set_event_callback_unsafe([](const netlib_event&){});
        sim->set_recv_callback_unsafe([](const char*,uint32_t){});
        sim->set_delay_callback_unsafe([](const char*,uint32_t){});

        dead_sockets.push_back(sim);

    }

    if(ui){
        ui->set_event_callback_unsafe([](const netlib_event&){});
        ui->set_recv_callback_unsafe([](const char*,uint32_t){});
        ui->set_delay_callback_unsafe([](const char*,uint32_t){});

        dead_sockets.push_back(ui);

    }

    disconnection_msg disconnected_p(client_id);
    send_to_connected_clients((char*)&disconnected_p, sizeof(disconnected_p),UI_CLIENT);

}

//sends current turn number followed by 
//replayed turns
void RtsServer::handle_replay_request(uint32_t client_id, uint32_t replay_point){

    auto& sim_client = clients[client_id][SIM_CLIENT]; 
    assert(sim_client);

#ifdef RTS_NETLIB_DEBUG
	BOOST_LOG_TRIVIAL(debug)<<"replay requst for: "<<client_id<<" from: "<<replay_point<<" replay_struct_size: "<<past_turns.size()<<" current turn: "<<turn_id;
#endif

    if(client_is_connected(client_id)){
        server_turn_info turn_info(turn_id);
        sim_client->send_data_unsafe((const char*)&turn_info, sizeof(turn_info));
        for(uint32_t i = replay_point; i < turn_id;i++){
            assert(past_turns[i]!=nullptr);
            const auto& l = past_turns[i]->get_turn_pkgs();
            for(const auto& pkg: l){
                sim_client->send_data_unsafe(pkg->data(),pkg->size());
            }      
        }
    }
}

void RtsServer::start_simulation(){
    current_turn = make_shared<Turn>(cache,turn_id);
    run_simulation();
}

void RtsServer::run_simulation(){
    server_state = SIMULATION;
    set_sim_data_callbacks();
    turn_timer.expires_from_now(boost::posix_time::milliseconds(game_pace.get_turn_len()));
    turn_timer.async_wait(bind(&RtsServer::turn_callback,this,placeholders::_1));
    netlib_msg started(SIMULATION_STARTED);
    send_to_connected_clients((const char *)&started,sizeof(started),UI_CLIENT);
}


void RtsServer::run(){
    //run the cleanup service
    a.async_accept(s,bind(&RtsServer::accept_handler,this,placeholders::_1));
}
