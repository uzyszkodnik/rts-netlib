
#include <cxxtest/TestSuite.h>
#include <boost/asio.hpp>
#include "../../src/comm_protocol.h"
#include "../../src/rts_netlib.h"
#include <string.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <thread>
#include <chrono>

using namespace std;
using namespace boost::asio::ip;
using namespace std;
using namespace std::placeholders;
using namespace boost::asio;




class SendTest : public CxxTest::TestSuite
{
    public:
        void testTurnFacilities(){
            srand(time(NULL));
            const uint32_t expected = 12345;
            BufferCache<char> cache(600, 576);
            BufferCache<turn_command> cmd_cache(1,12345);
            uint32_t turn_id = 0;
            Turn turn(cache,turn_id);
            TurnUnpacker unpacker(cache,cmd_cache); 

            char packet[expected][mtu];
            uint32_t size[expected];
            

            for(uint32_t i = 0 ; i < expected; i ++){
                size[i] = rand()%(max_cmd_size)+1;
                for(uint32_t j= 0 ; j<max_cmd_size;j++){
                    packet[i][j] = rand()% 255;
                }

                turn.add_command(i,packet[i],size[i]); 
            }

            auto l = turn.get_turn_pkgs();
            turn.set_expected_time(expected);
            bool broke = false;
            cout<<l.size()<<endl;

            for(auto& i : l){
                if(unpacker.process_packet(i->data(),i->size())){
                    broke = true;
                    break;
                }
            }
            TS_ASSERT(broke);
            TS_ASSERT( unpacker.get_schedule_time() == expected);

            auto cmds = unpacker.get_cmds();
            TS_ASSERT(cmds->size() == expected);
            for(uint32_t i = 0;i<expected;i++){       
                uint32_t expected_id = i;
                TS_ASSERT(cmds->at(i).player_id == expected_id);
                TS_ASSERT(cmds->at(i).size == size[i]);
                TS_ASSERT_SAME_DATA(packet[i],cmds->at(i).command->data(),size[i]);

            } 



    
}


        

};
