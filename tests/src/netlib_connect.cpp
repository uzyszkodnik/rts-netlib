
#include <cxxtest/TestSuite.h>
#include <boost/asio.hpp>
#include "../../src/comm_protocol.h"
#include "../../src/rts_netlib.h"
#include <string.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <thread>
#include <chrono>

using namespace std;
using namespace boost::asio::ip;
using namespace std;
using namespace std::placeholders;
using namespace boost::asio;




class SendTest : public CxxTest::TestSuite
{
    private:
    public:
        void testConnect(void){
            shared_ptr<io_service> io(new io_service());
            boost::asio::io_service::work work(*io);
            uint64_t pass = 0xDEADBEEF;
            bool success = true;
            std::vector<uint64_t> auth;
            auth.resize(1);
            auth[0]=pass;
            unique_ptr<RtsServer> s(new RtsServer(io,"1234", auth));
            auto io_thread = thread([&](){io->run();});
            s->run();
            unique_ptr<RtsUIClient> p(new RtsUIClient(io, 0, pass));
            p->set_event_callback([&](const netlib_event& ev){if(ev == DISCONNECTED){success=false;}});
            puts("connecting");
            p->connect("localhost","1234");
            puts("connected");
            //wait the dc period
            this_thread::sleep_for(chrono::seconds(3));
            io->stop();
            io_thread.join();
            TS_ASSERT(success); 

        }
        void testDisconnect(void){
            shared_ptr<io_service> io(new io_service());
            boost::asio::io_service::work work(*io);
            uint64_t pass = 0x12345;
            bool success = false;
            std::vector<uint64_t> auth;
            auth.resize(1);
            auth[0]=0x0;
            unique_ptr<RtsServer> s(new RtsServer(io,"1337", auth));
            auto io_thread = thread([&](){io->run();puts("finished running");});
            s->run();
            unique_ptr<RtsUIClient> p(new RtsUIClient(io, 0, pass));

            p->set_event_callback([&](const netlib_event& ev){if(ev == DISCONNECTED){puts("dc called");success=true;}});
            puts("connecting");
            p->connect("localhost","1337");
            puts("connected");
            //wait the dc period
            puts("sleep");
            this_thread::sleep_for(chrono::seconds(8));
            puts("stopping");
            io->stop();
            puts("joining");
            io_thread.join();
            puts("joined");
            TS_ASSERT(success); 

        }

        void testTurnLen(void){
            shared_ptr<io_service> io(new io_service());
            boost::asio::io_service::work work(*io);
            uint64_t pass = 0x12345;
            bool success = false;
            std::vector<uint64_t> auth;
            auth.resize(1);
            auth[0]=pass;
            unique_ptr<RtsServer> s(new RtsServer(io,"1337", auth));
            auto io_thread = thread([&](){io->run();puts("finished running");});
            s->run();
            unique_ptr<RtsSimClient> p(new RtsSimClient(io, 0, pass));

            p->set_event_callback([&](const netlib_event& ev){if(ev == DISCONNECTED){puts("dc called");success=true;}});
            puts("connecting");
            p->connect("localhost","1337");
            puts("connected");
            //wait the dc period
            puts("sleep");
            this_thread::sleep_for(chrono::seconds(8));
            puts("stopping");
            io->stop();
            puts("joining");
            io_thread.join();
            puts("joined");
            printf("turn len%u", p->get_server_turn_len());
        }

        

};
