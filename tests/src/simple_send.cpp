#include <cxxtest/TestSuite.h>
#include <boost/asio.hpp>
#include "../../src/comm_protocol.h"
#include <string.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <cstdlib>

using namespace std;
using namespace boost::asio::ip;
using namespace std;
using namespace std::placeholders;





class SendTest : public CxxTest::TestSuite
{
    public:
        void testSimpleSend(void)
        {

            std::shared_ptr<boost::asio::io_service> io_service(new boost::asio::io_service());
            CommLinkClient client(io_service);
            char msg[] = "all praise the darkness\n";
            CommLinkServer server(io_service, 1337, 200);
            client.connect("localhost","1337");
            server.set_recv_callback([&](const char *p, uint32_t size){

                    TS_ASSERT_EQUALS(size,strlen(msg));
                    TS_ASSERT_SAME_DATA(p, msg, strlen(msg)); 
                    io_service->stop();
                    });
            client.send_data(msg,strlen(msg));
            io_service->run();


        }

        
        void testSendMultiple(void)
        {
            std::shared_ptr<boost::asio::io_service> io_service(new boost::asio::io_service());

            CommLinkClient client(io_service) ;
            CommLinkServer server(io_service, 1337, 200);
            client.connect("localhost","1337");
            uint64_t packet_count = 10000;
            bool test_succeeded = true;
            server.set_recv_callback([&](const char *p, uint32_t size){

                    static uint64_t expected = 0 ;
                    uint64_t* m = (uint64_t*)p;

                    if(*m!=expected || size != sizeof(uint64_t)){
                        test_succeeded=false;
                    }
                    TS_ASSERT(*m == expected);
                    TS_ASSERT(size == sizeof(uint64_t));

                    expected++;

                    if(expected  == packet_count){
                        io_service->stop();
                    
                    }
            });
            for(uint64_t i =0 ; i < packet_count;i++){
                client.send_data((char*)&i, sizeof(i));         
            
            }

            io_service->run();
            TS_ASSERT(test_succeeded);
        }
        
};
