
#include <cxxtest/TestSuite.h>
#include <boost/asio.hpp>
#include "../../src/comm_protocol.h"
#include "../../src/rts_netlib.h"
#include <string.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <boost/asio.hpp>
#include <string.h>
#include <algorithm>
#include <vector>
#include <string.h>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <vector>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <cstring>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

using namespace std;
using namespace boost::asio::ip;
using namespace std;
using namespace std::placeholders;
using namespace boost::asio;


class Client{
    protected:
        unique_ptr<RtsUIClient> ui;
        unique_ptr<RtsSimClient> sim;
        thread ui_thread;
        thread sim_thread;
        bool ui_lanuched = false;
        uint32_t step_count;
    public:
        vector<turn_command> turns;

        Client(shared_ptr<io_service>& io, uint32_t id, uint64_t pass,uint32_t _steps=100):
            ui(new RtsUIClient(io,id,pass))
            ,sim(new RtsSimClient(io,id,pass))
            ,step_count(_steps)
    {
    }

        
        virtual void client_task(){
            this_thread::sleep_for(chrono::milliseconds(100)); 
            for(uint32_t i = 0 ; i < step_count; i++){
                char cmd[500]; 
                int cmd_s = rand()%500;
                for(int j = 0 ; j < cmd_s;j++){
                    cmd[j]=rand()%256; 
                }
                ui->send_command(cmd,cmd_s);
                this_thread::sleep_for(chrono::milliseconds(rand()%300)); 

            }
            puts("############ending ui#######");
            ui.release();

        }
        virtual void sim_task(){
            for(uint32_t i = 0 ; i < step_count;i++){

                uint64_t current_time = ms_since_epoch();
                auto cmds = sim->receive_commands(); 
                turns.insert(turns.end(),cmds->begin(),cmds->end()); 
                uint64_t time_left = ms_since_epoch() - current_time;
                BOOST_LOG_TRIVIAL(debug)<<"turn time: "<<time_left;
                TS_ASSERT(time_left > 90);
                // sim->free_cmds(cmds);
            }        

            puts("##############ending sim###############3");
            sim.release();

        }
        
        void start_sim(string ip, string port){
            ui->set_network_event_callback([=](const char* data,uint32_t len){
                    BOOST_LOG_TRIVIAL(debug)<<"received pkt of len: "<<len;
                    if(sizeof(netlib_msg)>=len && !ui_lanuched){
                        netlib_msg* p  = (netlib_msg*) data;
                        if(!ui_lanuched && p->type == SIMULATION_STARTED){
                            puts("started");
                            ui_lanuched=true;
                            ui_thread = thread(bind(&Client::client_task,this));
                        }
                    
                    }else if(sizeof(netlib_msg)>len){
                        puts("too small pkt received"); 
                    } 
                    
                    });
            puts("connecting ui");
            ui->connect(ip,port);
            puts("starting sim thread");

            sim->connect(ip,port); 
            sim_thread = thread(bind(&Client::sim_task,this));
            
        }

        void join(){
            sim_thread.join();
            ui_thread.join();
        }


};

class ContClient : public Client{
    private:

    public:
        vector<vector<char>> cmds;
        ContClient(shared_ptr<io_service>& io, uint32_t id, uint64_t pass,uint32_t cmd_count=100,uint32_t _steps=300): Client(io,id,pass,_steps){

            for(uint32_t i =0 ; i < cmd_count;i++){
                uint32_t size = rand()%max_cmd_size+1;
                vector<char> v(size);
                for(uint32_t j =0 ; j < size ; j++){
                    v[j]=rand()%255; 
                }
                cmds.push_back(v);
            }
        
        };

        void client_task(){
            this_thread::sleep_for(chrono::milliseconds(100));
            for(auto& cmd : cmds){
                ui->send_command(cmd.data(), cmd.size());
                this_thread::sleep_for(chrono::milliseconds(rand()%300));
            }
        
        }


};


class SlowClient: public ContClient{
    public:
        SlowClient(shared_ptr<io_service>& io, uint32_t id, uint64_t pass,uint32_t _cmd_count=100,uint32_t _steps=300): ContClient(io,id,pass,_cmd_count,_steps){};

        void sim_task(){
            for(uint32_t i = 0 ; i < step_count;i++){

                uint64_t current_time = ms_since_epoch();
                auto cmds = sim->receive_commands(); 
                turns.insert(turns.end(),cmds->begin(),cmds->end()); 
                uint64_t time_left = ms_since_epoch() - current_time;
                this_thread::sleep_for(chrono::milliseconds(90+rand()%200));
                BOOST_LOG_TRIVIAL(debug)<<"turn time: "<<time_left;
                // sim->free_cmds(cmds);
            }        
        }

};
class ContSlowClient: public ContClient{
    public:
        ContSlowClient(shared_ptr<io_service>& io, uint32_t id, uint64_t pass,uint32_t _cmd_count=100,uint32_t _steps=300): ContClient(io,id,pass,_cmd_count,_steps){};

        void sim_task(){
            for(uint32_t i = 0 ; i < step_count;i++){

                uint64_t current_time = ms_since_epoch();
                auto cmds = sim->receive_commands(); 
                turns.insert(turns.end(),cmds->begin(),cmds->end()); 
                uint64_t time_left = ms_since_epoch() - current_time;
                this_thread::sleep_for(chrono::milliseconds(200));
                BOOST_LOG_TRIVIAL(debug)<<"turn time: "<<time_left;
                // sim->free_cmds(cmds);
            }        
        }

};


class DcClient : public ContClient{
    public:
        DcClient(shared_ptr<io_service>& io, uint32_t id, uint64_t pass,uint32_t _cmd_count=100,uint32_t _steps=300): ContClient(io,id,pass,_cmd_count,_steps){};

        void sim_task(){
            uint32_t half_steps = step_count/2;
            auto c = cmds.begin(); 
            for(uint32_t i = 0 ; i < half_steps;i++){
                if(c != cmds.end()){
                    ui->send_command(c->data(),c->size());
                    c++;
                }
                uint64_t current_time = ms_since_epoch();
                auto s_cmds = sim->receive_commands(); 
                turns.insert(turns.end(),s_cmds->begin(),s_cmds->end()); 
                uint64_t time_left = ms_since_epoch() - current_time;
                BOOST_LOG_TRIVIAL(debug)<<"turn time: "<<time_left;



            }
            BOOST_LOG_TRIVIAL(debug)<<"disconnecting";
            ui->disconnect_quietly();
            sim->disconnect_quietly();
            BOOST_LOG_TRIVIAL(debug)<<"disconnected";
            this_thread::sleep_for(chrono::seconds(5));
            sim->reconnect();
            ui->reconnect();
            BOOST_LOG_TRIVIAL(debug)<<"reconnected";

            for(uint32_t i = half_steps ; i < step_count;i++){
                if(c != cmds.end()){
                    ui->send_command(c->data(),c->size());
                    c++;
                }
                uint64_t current_time = ms_since_epoch();
                auto s_cmds = sim->receive_commands(); 
                turns.insert(turns.end(),s_cmds->begin(),s_cmds->end()); 
                uint64_t time_left = ms_since_epoch() - current_time;
                BOOST_LOG_TRIVIAL(debug)<<"turn time: "<<time_left;



            }        
        }
        void client_task(){
        }

};

using namespace std;
using namespace boost::asio::ip;
using namespace std;
using namespace std::placeholders;
using namespace boost::asio;




class SendTest : public CxxTest::TestSuite
{
    private:
        vector<uint64_t> auth;
        shared_ptr<io_service> io;
        thread io_thread;
        shared_ptr<boost::asio::io_service::work> w ; 



        bool contains_ordered(vector<turn_command> cmds, vector<vector<char>> orders, uint32_t player_id){
            uint32_t current_order =0 ;
            cout<<"checking for player"<<player_id<<endl;
            for(auto& cmd: cmds){
                if(cmd.player_id == player_id){
                    if(cmd.size == orders[current_order].size() && memcmp(cmd.command->data(), orders[current_order].data(), cmd.size)==0){
                        current_order++; 
                    }else{
                        cout<<"wrong order cmd nr "<<current_order<<endl;
                        cout<<cmd.size<<" "<<orders[current_order].size()<<endl;
                        for(unsigned int i = 0 ; i < cmd.size;i++){
                            printf("%d ",*(cmd.command->data()+i)); 
                        }
                        puts("#######");
                        for(unsigned int i = 0 ; i < cmd.size;i++){
                            printf("%d ",orders[current_order][i]); 
                        }
                        puts("");
                        return false;
                    }

                }    
            }



            if(current_order == orders.size()){
                return true;
            }else{
                cout<<"not enough orders got:"<<current_order<<" expected: "<<orders.size()<<endl;
                return false; 
            }
        };
    public:
    
    //tests whether everyone is getting the same data
    SendTest(){
        srand(time(NULL));
        /*
        logging::add_file_log
    (
        keywords::file_name = "unit_netlib%N.log",
        keywords::rotation_size = 10 * 1024 * 1024,
        keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0),
        keywords::format = "[%TimeStamp%]: %Message%"
    );

    logging::core::get()->set_filter
    (
        logging::trivial::severity >= logging::trivial::debug
    );
    */
    }
    void setUp(){

        puts("setup called");
        io = make_shared<io_service>(); 
        w = make_shared<boost::asio::io_service::work>(*io);
        io_thread = thread([&](){io->run();puts("finished running");});

    };
    void tearDown(){
        puts("teardown called");
        io->stop();
        w.reset();
        io_thread.join();
        puts("teardown end");
    
    }

    void set_auth(uint32_t s){
        auth.resize(s);
        for(uint32_t i = 0 ; i<s;i++){
            auth[i]=i; 
        }

    }

    void testNormalCommSync(){

        const int player_count = 10;

        set_auth(player_count);
        unique_ptr<RtsServer> s(new RtsServer(io,"1337", auth));
        s->set_event_callback([](const netlib_event& ev){if(ev == SIMULATION_FINISHED){puts("end called");}});
        s->run();
        std::shared_ptr<Client> clients[player_count];


        for(int i =0; i< player_count;i++){
            puts("iter");
            clients[i] = make_shared<Client>(io,i,i);      
            clients[i]->start_sim("localhost","1337");
            puts("next iter");
        }
        puts("sleeping");
        for(int i = 0 ; i< player_count;i++){
            clients[i]->join(); 
        }
        auto t1 = clients[0]->turns;


        for(int i = 1 ; i < player_count;i++){
            auto t2 = clients[i]->turns;
            TS_ASSERT_EQUALS(t1.size(), t2.size());
            for(uint32_t i = 0 ; i < t1.size();i++){
                TS_ASSERT_EQUALS(t1[i].player_id, t2[i].player_id);
                TS_ASSERT_EQUALS(t1[i].size, t2[i].size); 
                if(t1[i].size > 0 ){
                    TS_ASSERT_SAME_DATA(t1[i].command->data(),t2[i].command->data(),t1[i].size);
                }
            
            }
        }

    }

    //tests whether commands were received in order they were sent
    void testDataCont(){
        const int player_count = 4;
        const int cmd_count = 100;
        const int step_count = cmd_count*2;
        puts("setting server");
        set_auth(player_count);
        std::shared_ptr<ContClient> clients[player_count];

        unique_ptr<RtsServer> s(new RtsServer(io,"1234", auth));
        s->set_event_callback([](const netlib_event& ev){if(ev == SIMULATION_FINISHED){puts("end called");}});

        s->run();

        puts("setting clients");
        for(int i = 0 ; i < player_count;i++){
            cout<<i<<endl;
            clients[i] = make_shared<ContClient>(io,i,i,cmd_count,step_count); 
            clients[i]->start_sim("localhost","1234");
        }

        puts("joining");
        for(int i = 0 ; i< player_count;i++){
            cout<<i<<endl;
            clients[i]->join(); 
        }
        puts("checking");
        for(int i =  0; i < player_count;i++){
            for(int j =0 ; j < player_count;j++){
                TS_ASSERT(contains_ordered(clients[j]->turns,clients[i]->cmds,i)); 
            }
        }
        puts("end1");
        puts("checking end");
    
    }


    void testReconnect(){
        const int player_count = 4;
        const int cmd_count = 100;
        puts("setting server");
        set_auth(player_count);
        std::shared_ptr<ContClient> clients[player_count-1];
        string port = "1891";
        unique_ptr<RtsServer> s(new RtsServer(io,port, auth));
        s->set_event_callback([](const netlib_event& ev){if(ev == SIMULATION_FINISHED){puts("end called");}});
        s->run();

        puts("setting clients");
        for(int i = 0 ; i < player_count-1;i++){
            cout<<i<<endl;
            clients[i] = make_shared<ContClient>(io,i,i,cmd_count,600); 
            clients[i]->start_sim("localhost",port);
        }
        DcClient c(io,3,3,500,600);
        c.start_sim("localhost",port);
        puts("joining");
        for(int i = 0 ; i< player_count-1;i++){
            cout<<i<<endl;
            clients[i]->join(); 
        }

        c.join();

        puts("checking");
        for(int i =  0; i < player_count-1;i++){
            for(int j =0 ; j < player_count-1;j++){
                TS_ASSERT(contains_ordered(clients[j]->turns,clients[i]->cmds,i)); 
            }
            TS_ASSERT(contains_ordered(c.turns,clients[i]->cmds,i));
        }

        for(int i = 0; i < player_count-1;i++){
             TS_ASSERT(contains_ordered(clients[i]->turns,c.cmds,3));
        }

        puts("checking end");

    }

    void testSlowDown(){
        const int player_count = 4;
        const int cmd_count = 100;
        puts("setting server");
        set_auth(player_count);
        std::shared_ptr<ContClient> clients[player_count];
        string port = "1890";
        unique_ptr<RtsServer> s(new RtsServer(io,port, auth));

        s->set_event_callback([](const netlib_event& ev){if(ev == SIMULATION_FINISHED){puts("end called");}});
        s->run();

        puts("setting clients");
        for(int i = 0 ; i < player_count;i++){
            cout<<i<<endl;
            clients[i] = make_shared<SlowClient>(io,i,i,cmd_count,200); 
            clients[i]->start_sim("localhost",port);
        }

        puts("joining");
        for(int i = 0 ; i< player_count;i++){
            cout<<i<<endl;
            clients[i]->join(); 
        }
        puts("checking");
        for(int i =  0; i < player_count;i++){
            for(int j =0 ; j < player_count;j++){
                TS_ASSERT(contains_ordered(clients[j]->turns,clients[i]->cmds,i)); 
            }
        }
        puts("checking end");

    }
    void testSingleslowDown(){
        const int player_count = 4;
        const int cmd_count = 100;
        puts("setting server");
        set_auth(player_count+1);
        std::shared_ptr<ContClient> clients[player_count];
        string port = "1890";
        unique_ptr<RtsServer> s(new RtsServer(io,port, auth));

        s->set_event_callback([](const netlib_event& ev){if(ev == SIMULATION_FINISHED){puts("end called");}});
        s->run();

        puts("setting clients");
        for(int i = 0 ; i < player_count;i++){
            cout<<i<<endl;
            clients[i] = make_shared<ContClient>(io,i,i,cmd_count,200); 
            clients[i]->start_sim("localhost",port);
        }
        ContSlowClient slower(io,player_count,player_count,cmd_count,200);
        slower.start_sim("localhost",port);

        puts("joining");
        for(int i = 0 ; i< player_count;i++){
            cout<<i<<endl;
            clients[i]->join(); 
        }
        slower.join();
        puts("checking");
        for(int i =  0; i < player_count;i++){
            cout<<"checking for cmd order for: " << i << endl;
            for(int j =0 ; j < player_count;j++){
                cout<<"checking recv in "<<j<<endl;
                TS_ASSERT(contains_ordered(clients[j]->turns,clients[i]->cmds,i)); 
            }
            cout<<"checking recv in slower"<<endl;
            TS_ASSERT(contains_ordered(slower.turns,clients[i]->cmds,i));
        }

        cout<<"checking send for slower"<<endl;
        for(int j =0 ; j < player_count;j++){

                cout<<"checking in : "<<j<<endl;
                TS_ASSERT(contains_ordered(clients[j]->turns,slower.cmds,player_count)); 
        }

        puts("checking end");
    
    }
};

