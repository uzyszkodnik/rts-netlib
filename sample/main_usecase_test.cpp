#include "../src/rts_netlib.h"
#include <boost/asio.hpp>
#include <memory>
#include <vector>
#include <stdlib.h>
#include <thread>
#include <chrono>
#include <algorithm>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>



using namespace std;

class Client{
    protected:
        unique_ptr<RtsSimClient> sim;
        unique_ptr<RtsUIClient> ui;
        int step_count;
        thread t;
        string addr,port;
        uint32_t _id;
    public:
        vector<turn_command> received_commands;
        vector<vector<char>> client_cmds; 

        Client(shared_ptr<boost::asio::io_service>& io, string _ip, string _port, uint32_t id, uint64_t pass, int _step_count=200): 
            step_count(_step_count)
            ,addr(_ip)
            ,port(_port)
            ,_id(id)
    
    {
            ui = unique_ptr<RtsUIClient>(new RtsUIClient(io,id,pass)); 
            sim = unique_ptr<RtsSimClient>(new RtsSimClient(io,id,pass));
            gen_cmds();
        }
        void start(){
            t = thread( [&](){
                        connect();                    
                        run();
                    });
        }

        void join(){
            t.join(); 
        }

        void connect(){
            sim->connect(addr,port);
            ui->connect(addr,port);
        }

        virtual void run(){
            for(int i = 0 ; i < step_count; i++){
                auto cmds = sim->receive_commands();
                received_commands.insert(received_commands.end(), cmds->begin(),cmds->end());
                if(i<step_count-1){
                    BOOST_LOG_TRIVIAL(info)<<_id<<" sending "<<client_cmds[i].size();
                    ui->send_command(client_cmds[i].data(),client_cmds[i].size());
                }
            }        
            sim->disconnect();
            ui->disconnect();
        
        }

        void gen_cmds(){
            int cmd_count = step_count - 1;
            client_cmds.resize(cmd_count);
            for(int i = 0 ; i < cmd_count;i++){
                int len = max(rand() % max_cmd_size,(unsigned int)1);         
                client_cmds[i].resize(len);
                for(int j = 0; j < len;j++){
                    client_cmds[i][j]=rand()%255; 
                }
            }
        
        }
};

class DisconnectingClient: public Client{
    public:
        DisconnectingClient(shared_ptr<boost::asio::io_service>& io, string _ip, string _port, uint32_t id, uint64_t pass, int _step_count=200): Client(io,_ip,_port,id,pass,_step_count) {};

        void run(){
            int steps_before_dc = step_count/2;
        
            for(int i = 0 ; i < steps_before_dc;i++){
                auto cmds = sim->receive_commands();
                received_commands.insert(received_commands.end(), cmds->begin(),cmds->end());
                ui->send_command(client_cmds[i].data(),client_cmds[i].size());

            
            }
            BOOST_LOG_TRIVIAL(info)<<"disconnecting";
            sim->disconnect();
            ui->disconnect();

            this_thread::sleep_for(chrono::seconds(5));
            BOOST_LOG_TRIVIAL(info)<<"disconnected";

            sim->reconnect();
            ui->reconnect();
            BOOST_LOG_TRIVIAL(info)<<"reconnected";

            for(int i = steps_before_dc;i<step_count;i++){
                auto cmds = sim->receive_commands();
                received_commands.insert(received_commands.end(), cmds->begin(),cmds->end());
                if(i<step_count-1)
                    ui->send_command(client_cmds[i].data(),client_cmds[i].size());
            }
        
        }

};

bool contains_ordered(vector<turn_command>& cmds, vector<vector<char>>& orders, uint32_t player_id){
            uint32_t current_order =0 ;
            cout<<"checking for player"<<player_id<<endl;
            for(auto& cmd: cmds){
                if(cmd.player_id == player_id){
                    if(cmd.size == orders[current_order].size() && memcmp(cmd.command->data(), orders[current_order].data(), cmd.size)==0){
                        current_order++; 
                    }else{
                        cout<<"wrong order cmd nr "<<current_order<<endl;
                        cout<<cmd.size<<" "<<orders[current_order].size()<<endl;
                        for(unsigned int i = 0 ; i < cmd.size;i++){
                            printf("%d ",*(cmd.command->data()+i)); 
                        }
                        puts("#######");
                        for(unsigned int i = 0 ; i < orders[current_order].size();i++){
                            printf("%d ",orders[current_order][i]); 
                        }
                        puts("");
                        return false;
                    }

                }    
            }

            return true;
}

int main(){
    srand(time(NULL));
    shared_ptr<boost::asio::io_service> io_service = make_shared<boost::asio::io_service>();
    boost::asio::io_service::work w(*io_service) ;

    const uint64_t client_count = 5;
    unique_ptr<Client> clients[client_count];
    vector<uint64_t> pass(client_count);
    const string addr  = "localhost";
    const string port = "1234";
    DisconnectingClient dc_client(io_service,addr,port,4,4);
    for(uint64_t i =0 ; i < client_count;i++){
        pass[i]=i; 
    
    }

    RtsServer server(io_service, port, pass,100,300,30,max_cmd_size);

    server.run();
    auto io_thread = thread([&](){io_service->run();});
    

    for(uint64_t i =0 ; i<client_count-1;i++){
        clients[i] = unique_ptr<Client>(new Client(io_service,addr,port,uint32_t(i),i));  
        clients[i]->start();
    }
    auto dc_id = client_count-1;
    clients[dc_id] = unique_ptr<DisconnectingClient>(new DisconnectingClient(io_service,addr,port,dc_id,dc_id));
    clients[dc_id]->start();


    for(uint64_t i = 0 ; i < client_count; i++){
        BOOST_LOG_TRIVIAL(info)<<"closing"<<i;
        clients[i]->join(); 
    }

    io_service->stop();
    io_thread.join();

    for(uint64_t i = 0 ; i < client_count;i++){
        for (uint64_t j = 0 ; j< client_count;j++){
            contains_ordered(clients[j]->received_commands,clients[i]->client_cmds,i);
        }

    }

}
